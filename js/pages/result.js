/*
global $
global ui
global device
global simpleDB
global deparam
global API
global Form
global Ready
global Click
global serializeObject
global config
*/
var result = {
    browseProcedure: function(t){
        $('#home [name="term"]').val($(t).text());
        $.mobile.changePage('#home');
        $('#home form').trigger('submit');
    },
    
    closeRating: function(t){
        $('#singleRating').dialog('close');
    },
    
    createRating: function(t){
        //if(confirm('Your name will be made public by submitting this rating.')) {
            var data = $(t).serializeObject();
            data.auth = window.key;
            var a = new API({
                db:false,
                data:data,
                success: function(){
                    $('body').trigger('createdRating');
                    ui.clearForm(t);
                    result.ratings();
                    $('#rating').dialog('close');
                }
            });
        //}
    },
    
    display: function(t){
        if(typeof device == 'undefined') {
            device = {
                platform:'web'
            }
        }
        var mapSize = '300x200';
        if($(window).width() > 300) {
            mapSize = '600x400';
        }
        var data = deparam($(t).attr('data-uri'));
        $('#theResult').attr('data-uri', $(t).attr('data-uri'));
        $('#result h1').html(data.name);
        var c = ','+String(data.coords).split(',').pop();
        var coords = String(data.coords).replace(c, '');
        var l = 'http://maps.google.com/maps/api/staticmap?center='+coords+'&maptype=roadmap&key=AIzaSyBX9cGqCsryk3Co_pH1BN9K64ZQR9k01lk&zoom=14&size='+mapSize+'&sensor=false&markers=size:mid%7Ccolor:0xff0000%7Clabel:1%7C'+coords;
        $('#map').html('<div class="text-center"><a href="'+l+'" class="extLink"><img src="'+l+'"></a></div>');
        $('#description').html('<h2>'+data.name+'</h2><p>'+data.description+'<br><br>'+(data.match ? '<b>Matching Service</b>: '+library.ucwords(data.match.service)+', $'+data.match.price+'<br><br>' : '')+
        (typeof data.tags == 'undefined' ? '' : '<b>Tags</b>: '+data.tags.join(', ')+'</p>'));
        $('#contact').html('<p><address>'+
        data.address+'</address><br><small>Disclaimer: The information in this app is provided for informational purposes only. Our data is updated every week, but please check with the '+
        'provider before making an appointment. Prices may change.</small><br><br>'+
        (data.link ? '<a class="ui-btn extLink" target="_blank" href="'+data.link+'">Visit Website</a><br>' : '')+
        '<a class="ui-btn" href="tel:'+data.phone+'">Call Provider</a><br>'+
        (String(device.platform).toLowerCase() == 'android' ? '<a class="ui-btn" href="geo:'+coords+'?q='+coords+'">Get Directions</a>' : 
        '<a class="ui-btn" href="maps://maps.apple.com/?q='+coords+'">Get Directions</a><br>')+
        '</p>');
        if(typeof data.services == 'undefined') {
            $('#services').html('<p>No results.</p>');
        } else {
            if(data.services.length) {
                var html = ''+
                '<ul data-role="listview" data-inset="true">'+
                '<li data-role="list-divider">Results <span class="ui-li-count">'+data.services.length+'</span></li>';
                var services = data.services.sort();
                $.each(services, function(key, value){
                    html += '<li>'+
                        '<img src="'+config.apiUrl+'?action=img&value=$'+data.prices[key]+'">'+
                        '<h2>'+value+'</h2>'+
                        '<p>Additional Service</p>'+
                    '</li>';
                });
                html += '</ul>';
                $('#services').html(html);
                $('#services ul').listview().listview('refresh');                
            } else {
                $('#services').html('<p>No results.</p>');
            }
        }
        result.ratings();
        $('#rating [name="provider"]').val(data.wp_id);
    },
    
    displayRating: function(t){
        var text = $(t).text();
        if(text != 'register' && text != 'log in') {
            var next = $(t).parents('li:first').next().find('a').attr('data-id');
            var uri = $(t).attr('data-uri');
            if(typeof uri != undefined) {
                var data = deparam(uri);
                var html = '<h3>'+data.name+'<br><small>'+data.rating+'/10</small></h3><p>'+data.comments+'</p>';
            }
            if(typeof next != 'undefined') {
                html += '<a data-id="'+next+'" href="#" id="nextRating">Next &raquo;</a>';
            }
            $('#singleRating .ui-content').html(html);
        }
    },

    init: function(){
        var db = new simpleDB('form');
        var form = db.get('rating');
        if(!form) {
            var a = new API({
                db:true,
                data:{
                    control:{
                        action:'get',
                        type:'form',
                        id:'rating'
                    }
                },
                success: function(response){
                    var db = new simpleDB('form');
                    db.put('rating', response.output);
                }
            });
        }
        
        var i = setInterval(function(){
            var db = new simpleDB('form');
            var form = db.get('rating');
            if(form) {
                clearInterval(i);
                var f = new Form({
                    bootstrap:false,
                    event: function(){
                        alert('');
                    },
                    form:form.form,
                    target:'#rating .ui-content',
                    name:'rating'
                }, function(f){
                    $('#'+f.id).find('button').parents('div:first').removeAttr('class');
                });
            }
        }, 1000);
    },
    
    nextRating: function(t){
        var id = $(t).attr('data-id');
        $('#ratings [data-id="'+id+'"]').trigger('click');
    },
    
    ratings: function(){
        if(window.key) {
            $.mobile.loading('show');
            $('#rate').attr('href', '#rating').removeClass('ui-state-disabled');
            var d = deparam($('#theResult').attr('data-uri'));
            var a = new API({
                db:false,
                suppressErrors:true,
                data:{
                    auth:window.key,
                    action:'ratings',
                    provider:d.wp_id
                },
                success:function(response){
                    d.rating = (d.rating == 'false' ? false : d.rating);
                    var html = ''+
                    '<ul data-role="listview" data-inset="true">'+
                    '<li data-role="list-divider">Overall <span class="ui-li-count">'+(d.rating ? d.rating : 'N\\A')+'</span></li>';
                    $.each(response.output, function(key, value){
                        var uri = $.param(value);
                        html += '<li>'+
                            '<a data-id="'+CryptoJS.SHA1(uri)+'" data-uri="'+uri+'" href="#singleRating" data-rel="dialog">'+
                            '<img src="'+config.apiUrl+'?action=img&value='+value.rating+'/10">'+
                            '<h2>'+value.name+'</h2>'+
                            '<p>'+value.comments+'</p>'+
                            '</a>'+
                        '</li>';
                    });
                    html += '</ul>';
                    $('#ratings').html(html);
                    $('#ratings ul').listview().listview('refresh');  
                    $.mobile.loading('hide');
                },
                error: function(){
                    $.mobile.loading('hide');
                    $('#ratings').html('<p>No Results.</p>');
                }
            });            
        } else {
            $('#rate').attr('href', '#rating').removeClass('ui-state-disabled').addClass('ui-state-disabled');
            $('#ratings').html('<p>Please <a data-rel="dialog" href="#register">register</a> or <a data-rel="dialog" href="#logIn">log in</a>.</p>');
        }
    }
};
Ready(result.init);
Click('[href="#result"]', result.display, false);
Submit('#rating form', result.createRating);
Click('#ratings a', result.displayRating, false);
Click('#nextRating', result.nextRating);
Click('#theProcedures a', result.browseProcedure);
$(document).on( "pagechange" , function(e, data) {
    var prev = $(data.prevPage).attr('id');
	var page = $('.ui-page-active').attr('id');
	if(page == 'result' && prev != 'singleRating') {
        $('[href="#location"]').trigger('click');
	}
});