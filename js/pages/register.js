var register = {
    go: function(t){
        $.mobile.loading('show');
        var d = $(t).serializeObject();
        var a = new API({
            db:false,
            data:d,
            error: function(){
                $.mobile.loading('hide');
            },
            success: function(response){
                logIn.success(response, '#register');
            }
        });
    },
    
    init: function(){
        
    }
};
Ready(register.init);
Submit('#register form', register.go);