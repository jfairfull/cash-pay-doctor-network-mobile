var drug = {
    candidate: function(t){
        var d = $(t).attr('data-drug');
        $('#drugPrice form [name="drug"]').val(d);
        $('#drugPrice form').submit();
    },
    
    search: function(t){
        if(window.key) {
            $.mobile.loading('show');
            var theData = $(t).serializeObject();
            theData.auth = window.key;
            var a = new API({
                db:false,
                data:theData,
                error: function(){
                    $.mobile.loading('hide');
                },
                success: function(response){
                    if(response.output['drug-info'].success) {
                        var html = '<h1>'+response.output['low-price'].data.display+' '+response.output['low-price'].data.form+' <small>'+response.output['low-price'].data.dosage+'</small></h1>';
                        html += '<h2>Fair Price</h2>';
                        html += '<a class="extLink" target="_blank" href="'+response.output['fair-price'].data.mobile_url+'">'+money_format('%i', response.output['fair-price'].data.price)+'</a>';
                        html += '<p>This is highest price you should pay for this medication at a local pharmacy, with or without insurance. '+
                        'This price usually represents a 30 day supply.</p>';
                        if(response.output['low-price']) {
                            html += '<h2>Lowest Prices</h2>';
                            html += '<ul>';
                            $.each(response.output['low-price'].data.price, function(key, value){
                                html += '<li><a class="extLink" href="'+response.output['low-price'].data.mobile_url+'">'+money_format('%i', value)+'</a> (qty. '+response.output['low-price'].data.quantity+')</li>';
                            });
                            html += '</ul>';
                            html += '<h2>Dosages</h2>';
                        }
                        if(response.output['drug-info']) {
                            $.each(response.output['drug-info'].data.drugs, function(form, drug){
                                html += '<h3>'+library.ucwords(form)+'</h3>';
                                html += '<ul>';
                                $.each(drug, function(dosage, url){
                                    html += '<li><a class="extLink" href="'+url+'" target="_blank">'+dosage+'</a> (available qtys. '+response.output['drug-info'].data.quantities[form][dosage]+')</li>';
                                });
                            html += '</ul>';
                            });
                        }
                        html += '<hr><div class="text-center"><a class="extLink" href="https://goodrx.com" target="_blank"><img width="120" height="50" src="img/goodrx.png" alt="GoodRx"></a></div>';
                        $('#drug').html(html);
                    } else {
                        if(typeof response.output['low-price'].errors != 'undefined') {
                            if(typeof response.output['low-price'].errors[0] != 'undefined') {
                                if(typeof response.output['low-price'].errors[0].candidates != 'undefined') {
                                    var html = '<ul data-role="listview" data-inset="true" id="candidates">';
                                    html += '<li data-role="list-divider">Did you mean:</li>';
                                    $.each(response.output['low-price'].errors[0].candidates, function(key, value){
                                        html += '<li><a data-drug="'+value+'" href="#">'+value+'</a></li>';
                                    });
                                    html += '</ul>';
                                    $('#drug').html(html);
                                    $('#candidates').listview().listview('refresh');
                                }
                            }
                        }
                    }
                    $.mobile.loading('hide');
                }
            });
        } else {
            alert('Please register or log in to search for drugs.');
            window.location.hash = 'menu';
        }
    }
};
Submit('#drugPrice form', drug.search);
Click('#candidates a', drug.candidate);