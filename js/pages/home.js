var home = {
    format: function(results){
        var o = {};
        $.each(results, function(key, value){
            o[value.wp_id] = value;
        });
        return o;
    },
    
    results: function(response, sel){
        if(typeof sel == 'undefined') {
            sel = '#results';
        }
        $(sel+' ul').html('<li data-role="list-divider">Search Results <span class="ui-li-count">'+response.output.length+'</span></li>');
        $.each(response.output, function(key, value){
            var html = '<li>'+
            '<a data-uri="'+$.param(value)+'" data-id="'+value.wp_id+'" href="#result">';
            if(value.match) {
                html += '<img src="'+config.apiUrl+'?action=img&value=$'+value.match.price+'">';
            }
            html += '<h2>'+value.name+'</h2>'+
            '<p>'+(value.match ? value.match.service : value.description)+'</p>'+
            '</a>'+
            '</li>';
            $(sel+' ul').append(html);
        });
        $(sel+' ul').listview().listview('refresh');
    },
    
    search: function(t){
        $.mobile.loading('show');
        var a = new API({
            db:false,
            data:$(t).serializeObject(),
            success: function(response){
                home.results(response);
                $.mobile.loading('hide');
            },
            error: function(){
                alert('No Results.');
                $.mobile.loading('hide');
            }
        });
    }
};
Submit('#home form', home.search);