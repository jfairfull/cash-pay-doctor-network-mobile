var logIn = {
    go: function(t){
        $.mobile.loading('show');
        var d = $(t).serializeObject();
        var a = new API({
            db:false,
            data:d,
            success: function(response){
                logIn.success(response, '#logIn');
            },
            error: function(){
                $.mobile.loading('hide');
            }
        });
    },
    
    success: function(response, d){
        var db = new simpleDB('user');
        db.put('data', response.output.user);
        window.key = response.output.user.key;
        $(d).dialog('close');
        library.populateForm('#options form', response.output.user);
        $('#ratings').html('No results.');
        global.ui();
        $('body').trigger('logIn');
        var uri = $('#theResult').attr('data-uri');
        if(typeof uri != 'undefined') {
            if(uri) {
                result.ratings();
            }
        }
        $.mobile.loading('hide');        
    }
};
Submit('#logIn form', logIn.go);