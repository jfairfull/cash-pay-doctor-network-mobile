var concierge = {
    display: function(response){
        $('#concierge .list').html('');
        var html = ''+
        '<ul data-role="listview" data-inset="true">'+
        '<li data-role="list-divider">Results <span class="ui-li-count">'+response.output.length+'</span></li>';
        $.each(response.output, function(key, value){
            html += '<li>'+
            '<a data-uri="'+$.param(value)+'" data-id="'+value.wp_id+'" href="#result">'+
            '<h2>'+value.name+'</h2>'+
            '<p>Subscription Provider</p>'+
            '</a>'+
            '</li>';
        });
        html += '</ul>';
        $('#concierge .list').html(html);
        $('#concierge .list ul').listview().listview('refresh');    
        $.mobile.loading('hide');
    },
    
    sort: function(t){
        $.mobile.loading('show');
        var zip = $(t).find('[name="zip"]').val();
        var a = new API({
            db:false,
            data:{
                action:'concierge',
                zip:zip
            },
            success: function(response){
                concierge.display(response);
            }
        });
    }
};
Submit('#concierge form', concierge.sort);