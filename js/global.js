var global = {
    categories: function(){
        new API({
            db:false,
            data:{
                action:'browseCategories',
                noChildren:'true'
            },
            success: function(response){
                var html = '<ul data-role="listview" data-autodividers="true" data-inset="true">';
                $.each(response.output, function(key, value){
                    html += '<li><a data-id="'+key+'" href="#category">'+value+'</a></li>';
                });
                $('#browse .ui-content').html(html+'</ul>');
                $('#browse ul').listview().listview('refresh');
            }
        });
    },
    
    category: function(t){
        $.mobile.loading('show');
        $('#theCategory .ui-content').html('');
        $('#category h1').text($(t).text());
        new API({
            db:false,
            data:{
                action:'getCategory',
                id:$(t).attr('data-id')
            },
            success: function(response){
                var html = '<ul data-role="listview" data-autodividers="true" data-inset="true">';
                if(typeof response.output.category.children != 'undefined') {
                    $.each(response.output.category.children, function(key, value){
                        html += '<li><a class="subcategory" data-id="'+key+'" href="#">'+value.name+'</a></li>';
                    });
                } else {
                    if(response.output.codes) {
                        $.each(response.output.codes, function(key, value){
                            html += '<li><a class="code" data-id="'+value.id+'" href="#">'+value.friendlyName+'</a></li>';
                        });
                    }                    
                }
                $('#theCategory').html(html+'</ul>');
                $('#theCategory ul').listview().listview('refresh'); 
                $.mobile.changePage("#category");
                window.scrollTo(0, 0);                
                $.mobile.loading('hide');
            }
        });
    },
    
    code: function(t){
        var zip = $('#category [name="zip"]').val();
        if(zip) {
            $.mobile.loading('show');
            new API({
                db:false,
                data:{
                    action:'getCode',
                    id:$(t).attr('data-id'),
                    zip:zip
                },
                success: function(response){
                    home.results(response, '#code .ui-content');
                    $.mobile.changePage("#code");
                    $.mobile.loading('hide');
                }
            });
        } else {
            alert('Please enter your zip.');
        }
    },
    
    disableAds: function(){
        disableAds.purchase('hm_disable_ads');
    },
    
    editUser: function(t){
        if(window.key) {
            $.mobile.loading('show');
            var data = $(t).serializeObject();
            data.auth = window.key;
            data.action = 'editUser';
            var a = new API({
                db:false,
                data:data,
                error: function(){
                    $.mobile.loading('hide');
                },
                success: function(){
                    alert('Account edited successfully.');
                    $.mobile.loading('hide');
                }
            });
        } else {
            alert('Please log in first.');
        }
    },
    
    hcpcs: function(){
        var a = new API({
            db:false,
            data:{
                action:'hcpcs'
            },
            success: function(response){
                var html = '<ul data-role="listview" data-autodividers="true" data-inset="true">';
                $.each(response.output, function(key, value){
                    html += '<li><a href="#">'+value+'</a></li>';
                });
                $('#theProcedures').html(html+'</ul>');
                $('#theProcedures ul').listview().listview('refresh');                 
            }
        });
    },
    
    init: function(){
        $(function() {
            FastClick.attach(document.body);
        });
        global.joinForm();
        // global.hcpcs();
        global.categories();
        var a = new API({
            db:false,
            data:{
                action:'terms'
            },
            success: function(response){
                $('[name="term"]').autocomplete({
                    source: response.output,
                    minLength: 3,
                    delay: 500
                });
            }
        });
        var db = new simpleDB('user');
        var d = db.get('data');
        if(d) {
            window.key = d.key;
            library.populateForm('#options form', d);
            $('body').find('[name="zip"]').each(function(){
                $(this).val(d.zip);
            });
        }
        global.ui();
        if(cordovaApp) {
            app.initialize();
        }
    },
    
    join: function(t){
        $.mobile.loading('show');
        var a = new API({
            db:false,
            data:$(t).serializeObject(),
            error: function(){
                alert('There was a problem with your submission, please try again.');
                $.mobile.loading('hide');
            },
            success: function(response){
                $('#join').dialog('close');
                $.mobile.loading('hide');
                alert('Thank you, we will be in touch.');
            }
        });
    },
    
    joinForm: function(){
        var db = new simpleDB('form');
        var form = db.get('join');
        if(!form) {
            var a = new API({
                db:true,
                data:{
                    control:{
                        action:'get',
                        type:'form',
                        id:'join'
                    }
                },
                success: function(response){
                    var db = new simpleDB('form');
                    db.put('join', response.output);
                }
            });            
        }   
        var i = setInterval(function(){
            var db = new simpleDB('form');
            var form = db.get('join');
            if(form) {
                clearInterval(i);
                var f = new Form({
                    bootstrap:false,
                    event: function(){},
                    form:form.form,
                    target:'#join .ui-content',
                    name:'join'
                }, function(f){
                    $('#'+f.id).find('button').parents('div:first').removeAttr('class');
                });
            }
        }, 1000);        
    },
    
    ui: function(){
        if(window.key) {
            $('#rate').attr('href', '#rating').removeClass('ui-state-disabled');
            $('[href="#logIn"], [href="#register"]').addClass('ui-state-disabled');
            $('#drug').html('');
        } else {
            $('#rate').attr('href', '#rating').removeClass('ui-state-disabled').addClass('ui-state-disabled');;
            $('[href="#logIn"], [href="#register"]').removeClass('ui-state-disabled');
            $('#drug').html('<p>Please <a data-rel="dialog" href="#register">register</a> or <a data-rel="dialog" href="#logIn">log in</a>.</p>');
        }
    }
};
$(function() {
    FastClick.attach(document.body);
});
Ready(global.init);
Submit('#join form', global.join);
Submit('#options form', global.editUser);
Click('#browse .ui-listview a, #category .ui-listview .subcategory', global.category);
Click('#category .ui-listview .code', global.code);
Click('#disableAds', global.disableAds);