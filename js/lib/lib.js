/*
CryptoJS v3.1.2
code.google.com/p/crypto-js
(c) 2009-2013 by Jeff Mott. All rights reserved.
code.google.com/p/crypto-js/wiki/License
*/
var CryptoJS=CryptoJS||function(e,m){var p={},j=p.lib={},l=function(){},f=j.Base={extend:function(a){l.prototype=this;var c=new l;a&&c.mixIn(a);c.hasOwnProperty("init")||(c.init=function(){c.$super.init.apply(this,arguments)});c.init.prototype=c;c.$super=this;return c},create:function(){var a=this.extend();a.init.apply(a,arguments);return a},init:function(){},mixIn:function(a){for(var c in a)a.hasOwnProperty(c)&&(this[c]=a[c]);a.hasOwnProperty("toString")&&(this.toString=a.toString)},clone:function(){return this.init.prototype.extend(this)}},
n=j.WordArray=f.extend({init:function(a,c){a=this.words=a||[];this.sigBytes=c!=m?c:4*a.length},toString:function(a){return(a||h).stringify(this)},concat:function(a){var c=this.words,q=a.words,d=this.sigBytes;a=a.sigBytes;this.clamp();if(d%4)for(var b=0;b<a;b++)c[d+b>>>2]|=(q[b>>>2]>>>24-8*(b%4)&255)<<24-8*((d+b)%4);else if(65535<q.length)for(b=0;b<a;b+=4)c[d+b>>>2]=q[b>>>2];else c.push.apply(c,q);this.sigBytes+=a;return this},clamp:function(){var a=this.words,c=this.sigBytes;a[c>>>2]&=4294967295<<
32-8*(c%4);a.length=e.ceil(c/4)},clone:function(){var a=f.clone.call(this);a.words=this.words.slice(0);return a},random:function(a){for(var c=[],b=0;b<a;b+=4)c.push(4294967296*e.random()|0);return new n.init(c,a)}}),b=p.enc={},h=b.Hex={stringify:function(a){var c=a.words;a=a.sigBytes;for(var b=[],d=0;d<a;d++){var f=c[d>>>2]>>>24-8*(d%4)&255;b.push((f>>>4).toString(16));b.push((f&15).toString(16))}return b.join("")},parse:function(a){for(var c=a.length,b=[],d=0;d<c;d+=2)b[d>>>3]|=parseInt(a.substr(d,
2),16)<<24-4*(d%8);return new n.init(b,c/2)}},g=b.Latin1={stringify:function(a){var c=a.words;a=a.sigBytes;for(var b=[],d=0;d<a;d++)b.push(String.fromCharCode(c[d>>>2]>>>24-8*(d%4)&255));return b.join("")},parse:function(a){for(var c=a.length,b=[],d=0;d<c;d++)b[d>>>2]|=(a.charCodeAt(d)&255)<<24-8*(d%4);return new n.init(b,c)}},r=b.Utf8={stringify:function(a){try{return decodeURIComponent(escape(g.stringify(a)))}catch(c){throw Error("Malformed UTF-8 data");}},parse:function(a){return g.parse(unescape(encodeURIComponent(a)))}},
k=j.BufferedBlockAlgorithm=f.extend({reset:function(){this._data=new n.init;this._nDataBytes=0},_append:function(a){"string"==typeof a&&(a=r.parse(a));this._data.concat(a);this._nDataBytes+=a.sigBytes},_process:function(a){var c=this._data,b=c.words,d=c.sigBytes,f=this.blockSize,h=d/(4*f),h=a?e.ceil(h):e.max((h|0)-this._minBufferSize,0);a=h*f;d=e.min(4*a,d);if(a){for(var g=0;g<a;g+=f)this._doProcessBlock(b,g);g=b.splice(0,a);c.sigBytes-=d}return new n.init(g,d)},clone:function(){var a=f.clone.call(this);
a._data=this._data.clone();return a},_minBufferSize:0});j.Hasher=k.extend({cfg:f.extend(),init:function(a){this.cfg=this.cfg.extend(a);this.reset()},reset:function(){k.reset.call(this);this._doReset()},update:function(a){this._append(a);this._process();return this},finalize:function(a){a&&this._append(a);return this._doFinalize()},blockSize:16,_createHelper:function(a){return function(c,b){return(new a.init(b)).finalize(c)}},_createHmacHelper:function(a){return function(b,f){return(new s.HMAC.init(a,
f)).finalize(b)}}});var s=p.algo={};return p}(Math);
(function(){var e=CryptoJS,m=e.lib,p=m.WordArray,j=m.Hasher,l=[],m=e.algo.SHA1=j.extend({_doReset:function(){this._hash=new p.init([1732584193,4023233417,2562383102,271733878,3285377520])},_doProcessBlock:function(f,n){for(var b=this._hash.words,h=b[0],g=b[1],e=b[2],k=b[3],j=b[4],a=0;80>a;a++){if(16>a)l[a]=f[n+a]|0;else{var c=l[a-3]^l[a-8]^l[a-14]^l[a-16];l[a]=c<<1|c>>>31}c=(h<<5|h>>>27)+j+l[a];c=20>a?c+((g&e|~g&k)+1518500249):40>a?c+((g^e^k)+1859775393):60>a?c+((g&e|g&k|e&k)-1894007588):c+((g^e^
k)-899497514);j=k;k=e;e=g<<30|g>>>2;g=h;h=c}b[0]=b[0]+h|0;b[1]=b[1]+g|0;b[2]=b[2]+e|0;b[3]=b[3]+k|0;b[4]=b[4]+j|0},_doFinalize:function(){var f=this._data,e=f.words,b=8*this._nDataBytes,h=8*f.sigBytes;e[h>>>5]|=128<<24-h%32;e[(h+64>>>9<<4)+14]=Math.floor(b/4294967296);e[(h+64>>>9<<4)+15]=b;f.sigBytes=4*e.length;this._process();return this._hash},clone:function(){var e=j.clone.call(this);e._hash=this._hash.clone();return e}});e.SHA1=j._createHelper(m);e.HmacSHA1=j._createHmacHelper(m)})();

function SelectorCheck(selector) {
    /*
    setTimeout(function() {
        if (!$(selector).length) {
            console.warn('Template Warning: ' + selector + ' does not exist on this page.');
        }
    }, 3000);
    */
}

function Blur(selector, action) {
    SelectorCheck(selector);
    $(document).on('blur', selector, function(e) {
        if (typeof action == 'function') {
            action(this, e);
        }
    });
}

function Change(selector, action, preventDefault) {
    var p = (typeof preventDefault == 'undefined' ? true : preventDefault);
    SelectorCheck(selector);
    $(document).on('change', selector, function(e) {
        if (p) {
            e.preventDefault();
        }
        if (typeof action == 'function') {
            action(this, e);
        }
    });
}

function Click(selector, action, preventDefault) {
    var p = (typeof preventDefault == 'undefined' ? true : preventDefault);
    SelectorCheck(selector);
    $(document).on('click', selector, function(e) {
        if (p) {
            e.preventDefault();
        }
        if (typeof action == 'function') {
            action(this, e);
        }
    });
}

function Focus(selector, action) {
    SelectorCheck(selector);
    $(document).on('focus', selector, function(e) {
        if (typeof action == 'function') {
            action(this, e);
        }
    });
}

function Keydown(selector, action) {
    SelectorCheck(selector);
    $(document).on('keydown', selector, function(e) {
        if (typeof action == 'function') {
            action(this, e);
        }
    });
}

function Keyup(selector, action) {
    SelectorCheck(selector);
    $(document).on('keyup', selector, function(e) {
        if (typeof action == 'function') {
            action(this, e);
        }
    });
}

function Load(action) {
    $(window).load(function() {
        if (typeof action == 'function') {
            action(this);
        }
    });
}

function Ready(action) {
    $(document).ready(function() {
        if (typeof action == 'function') {
            action();
        }
    });
}

function Submit(selector, action) {
    SelectorCheck(selector);
    $(document).on('submit', selector, function(e) {
        e.preventDefault();
        if (typeof action == 'function') {
            action(this, e);
        }
    });
};
var api = {
    url: location.protocol + '//' + location.hostname + '/api/',
    compressOutput: false,
    call: function(data, callback, error) {
        if (typeof data == 'object') {
            var d = $.param(data);
            data = d;
        }
        if (typeof api.licenseKey != 'undefined') {
            data[licenseKey] = api.licenseKey;
        }
        if (typeof config != 'undefined') {
            if (String(data).indexOf('_CONTROL') != -1) {
                api.url = config.dbUrl;
            } else {
                api.url = config.apiUrl;
            }
        }
        if (typeof parent.config != 'undefined') {
            if (String(data).indexOf('_CONTROL') != -1) {
                api.url = parent.config.dbUrl;
            } else {
                api.url = parent.config.apiUrl;
            }
        }
        if (data.length < 7000) {
            $.getJSON(api.url + '?jsoncallback=?' + '&' + data + (typeof api.licenseKey == 'undefined' ? '' : '&licenseKey=' + api.licenseKey +
                (api.compressOutput ? '&compressOutput=' : '')), function(response) {
                if (library.processResponse(response)) {
                    if (typeof callback !== 'undefined') {
                        callback(response);
                    }
                } else {
                    if (typeof error !== 'undefined') {
                        error(response);
                    }
                }
            });
        } else {
            $.post(api.url + '?' + data + (typeof api.licenseKey == 'undefined' ? '' : '&licenseKey=' + api.licenseKey +
                (api.compressOutput ? '&compressOutput=' : '')), function(response) {
                if (library.processResponse(response)) {
                    if (typeof callback !== 'undefined') {
                        callback(response);
                    }
                } else {
                    if (typeof error !== 'undefined') {
                        error(response);
                    }
                }
            });
        }
    },
    dbCall: function(data, callback, error) {
        $.getJSON(dbUrl + '?jsoncallback=?' + '&' + data, function(response) {
            if (library.processResponse(response)) {
                if (typeof callback !== 'undefined') {
                    callback(response);
                }
            } else {
                if (typeof error !== 'undefined') {
                    error(response);
                }
            }
        });
    },
    isLoggedIn: function(success, error) {
        var request = new Request({
            control: {
                action: 'checkSession'
            }
        });
        api.call(request.output(), function(response) {
            if (response.output.logged_in) {
                if (typeof success != 'undefined') {
                    success();
                }
            } else {
                if (typeof error != 'undefined') {
                    error();
                }
            }
        });
    },
    send: function(data, callback, error) {
        $.ajax({
            url: 'php/upload.php',
            data: data,
            cache: false,
            contentType: false,
            processData: false,
            type: 'POST',
            success: function(data) {
                alert(data);
            }
        });
    }
};
(function(window) {
    var undef;
    var decode = decodeURIComponent;
    window.deparam = function(text, reviver) {
        var result = {};
        text.replace(/\+/g, ' ').split('&').forEach(function(pair, index) {
            var kv = pair.split('=');
            var key = decode(kv[0]);
            if (!key) {
                return;
            }
            var value = decode(kv[1] || '');
            var keys = key.split('][');
            var last = keys.length - 1;
            var i = 0;
            var current = result;
            if (keys[0].indexOf('[') >= 0 && /\]$/.test(keys[last])) {
                keys[last] = keys[last].replace(/\]$/, '');
                keys = keys.shift().split('[').concat(keys);
                last++;
            } else {
                last = 0;
            }
            if (typeof reviver === 'function') {
                value = reviver(key, value);
            } else if (reviver) {
                value = deparam.reviver(key, value);
            }
            if (last) {
                for (; i <= last; i++) {
                    key = keys[i] !== '' ? keys[i] : current.length;
                    if (i < last) {
                        current = current[key] = current[key] || (isNaN(keys[i + 1]) ? {} : []);
                    } else {
                        current[key] = value;
                    }
                }
            } else {
                if (Array.isArray(result[key])) {
                    result[key].push(value);
                } else if (key in result) {
                    result[key] = [result[key], value];
                } else {
                    result[key] = value;
                }
            }
        });
        return result;
    };
    deparam.reviver = function(key, value) {
        var specials = {
            'true': true,
            'false': false,
            'null': null,
            'undefined': undef
        };
        return (+value + '') === value ? +value : value in specials ? specials[value] : value;
    };
}(this));;

function Request(i) {
    this.control = false;
    this.data = false;
    if (typeof i.control == 'undefined') {
        this.control = i;
    } else {
        this.control = i.control;
    }
    if (typeof i.data != 'undefined') {
        if (typeof i.control == 'undefined') {
            i.control = {}
            this.control = {}
        } else {
            this.data = i.data;
        }
    }
    this.output = function() {
        var output = {}
        if (this.control) {
            output.$_CONTROL = this.control;
        }
        if (this.data) {
            output.$_DATA = this.data;
        }
        return $.param(output);
    }
};
(function(jQuery) {
    jQuery.fn.serializeObject = function(options) {
        options = jQuery.extend({}, options);
        var self = this,
            json = {},
            push_counters = {},
            patterns = {
                "validate": /^[a-zA-Z][a-zA-Z0-9_]*(?:\[(?:\d*|[a-zA-Z0-9_]+)\])*$/,
                "key": /[a-zA-Z0-9_]+|(?=\[\])/g,
                "push": /^$/,
                "fixed": /^\d+$/,
                "named": /^[a-zA-Z0-9_]+$/
            };
        this.build = function(base, key, value) {
            base[key] = value;
            return base;
        };
        this.push_counter = function(key) {
            if (push_counters[key] === undefined) {
                push_counters[key] = 0;
            }
            return push_counters[key]++;
        };
        jQuery.each(jQuery(this).serializeArray(), function() {
            if (!patterns.validate.test(this.name)) {
                return;
            }
            var k, keys = this.name.match(patterns.key),
                merge = this.value,
                reverse_key = this.name;
            while ((k = keys.pop()) !== undefined) {
                reverse_key = reverse_key.replace(new RegExp("\\[" + k + "\\]$"), '');
                if (k.match(patterns.push)) {
                    merge = self.build([], self.push_counter(reverse_key), merge);
                } else if (k.match(patterns.fixed)) {
                    merge = self.build([], k, merge);
                } else if (k.match(patterns.named)) {
                    merge = self.build({}, k, merge);
                }
            }
            json = jQuery.extend(true, json, merge);
        });
        return json;
    }
})(jQuery);;

function API(i) {
    this.p = {
        compress: true,
        db: false,
        data: {},
        error: function(response) {
            console.log(response);
        },
        fileUpload: false,
        license: false,
        post: false,
        success: function(response) {
            console.log(response);
        },
        suppressErrors: false,
        url: location.protocol + '//' + location.host + '/api/'
    }
    var that = this;
    this.options = $.extend(that.p, i);
    if (typeof this.options.data !== 'object') {
        this.options.data = deparam(this.options.data);
    }
    if (this.options.license) {
        this.options.data.licenseKey = this.options.license;
    }
    if (this.options.compress) {
        this.options.data.compressOutput = 'true';
    }
    if (this.p.url == location.protocol + '//' + location.host + '/api/') {
        if (typeof config != 'undefined') {
            this.options.url = config.apiUrl;
        }
    }
    var theRequest = '';
    if (this.options.db) {
        this.options.url = location.protocol + '//' + location.host + '/db/';
        var request = new Request(this.options.data);
        this.options.data = request.output();
        if (typeof parent.config != 'undefined') {
            this.options.url = parent.config.dbUrl;
        }
        if (typeof config != 'undefined') {
            this.options.url = config.dbUrl;
        }
        theRequest = this.options.url + '?jsoncallback=?&' + this.options.data;
    }
    var that = this;
    var data = '';
    var callback = this.options.success;
    var error = this.options.error;
    if (!this.options.fileUpload && !this.options.db) {
        var data = $.param(that.options.data);
        theRequest = this.options.url + '?jsoncallback=?&' + data;
    }
    if (data.length > 7000 || theRequest.length > 7000) {
        this.options.post = true;
    }
    if (this.options.fileUpload) {
        $.ajax({
            url: that.options.url,
            data: that.options.data,
            cache: false,
            contentType: false,
            processData: false,
            type: 'POST',
            success: function(response) {
                if (library.processResponse(response, that.options.suppressErrors)) {
                    callback(response);
                } else {
                    error(response);
                }
            }
        });
    } else if (this.options.post) {
        $.post(this.options.url, this.options.data, function(response) {
            if (library.processResponse(response, that.options.suppressErrors)) {
                callback(response);
            } else {
                error(response);
            }
        });
    } else {
        $.getJSON(theRequest, function(response) {
            if (library.processResponse(response, that.options.suppressErrors)) {
                callback(response);
            } else {
                error(response);
            }
        });
    }
};
if (typeof Object.keys != 'function') {
    Object.keys = function(obj) {
        if (typeof obj != 'object') {
            obj = library.toObject(obj);
        }
        var ret = [];
        for (var x in obj) {
            if (obj.hasOwnProperty(x)) {
                ret[ret.length] = x;
            }
        }
        return ret;
    }
}
var library = {
    chunk: function(obj, size) {
        var a = []
        var b = []
        $.each(obj, function(key, value) {
            a.push(value);
        });
        for (var i = 0; i < a.length; i += size) {
            b.push(a.slice(i, i + size));
        }
        return b;
    },
    clearForm: function(form) {
        $(form).find('input').each(function() {
            var type = $(this).attr('type');
            switch (type) {
                case 'submit':
                    break;
                case 'checkbox':
                    $(this).attr('checked', false);
                    break;
                case 'radio':
                    $(this).attr('checked', false);
                    break;
                case 'hidden':
                    break;
                case 'select':
                    break;
                default:
                    $(this).val('');
                    break;
            }
            $(form).find('textarea').each(function() {
                $(this).val('');
            });
            $(form).find('select').each(function() {
                $(this).find('option:first').attr('selected', 'selected');
            });
        });
    },
    delay: function() {
        var timer = 0;
        return function(callback, ms) {
            clearTimeout(timer);
            timer = setTimeout(callback, ms);
        };
    }(),
    displayObject: function(obj) {
        var u = ['file', 'timestamp', 'parent', 'id', 'type']
        var d = jQuery.extend({}, obj);
        $.each(u, function(key, value) {
            if (obj.hasOwnProperty(value)) {
                delete obj[value];
            }
        });
        return '<pre> ' + $.trim(String(JSON.stringify(obj, null, 1)).replace(/"/g, '').replace('}', '').replace('{', '').replace(/\r,/g, '')) + '</pre>';
    },
    extractTextWithWhitespace: function(elems) {
        var lineBreakNodeName = "BR";
        if ($.browser.webkit) {
            lineBreakNodeName = "DIV";
        } else if ($.browser.msie) {
            lineBreakNodeName = "P";
        } else if ($.browser.mozilla) {
            lineBreakNodeName = "BR";
        } else if ($.browser.opera) {
            lineBreakNodeName = "P";
        }
        var extractedText = extractTextWithWhitespaceWorker(elems, lineBreakNodeName);
        return extractedText;
    },
    extractTextWithWhitespaceWorker: function(elems, lineBreakNodeName) {
        var ret = "";
        var elem;
        for (var i = 0; elems[i]; i++) {
            elem = elems[i];
            if (elem.nodeType === 3 || elem.nodeType === 4) {
                ret += elem.nodeValue;
            }
            if (elem.nodeName === lineBreakNodeName) {
                ret += "\n";
            }
            if (elem.nodeType !== 8) {
                ret += library.extractTextWithWhitespace(elem.childNodes, lineBreakNodeName);
            }
        }
        return ret;
    },
    getCookie: function(c_name) {
        var c_value = " " + document.cookie;
        var c_start = c_value.indexOf(" " + c_name + "=");
        if (c_start == -1) {
            c_value = null;
        } else {
            c_start = c_value.indexOf("=", c_start) + 1;
            var c_end = c_value.indexOf(";", c_start);
            if (c_end == -1) {
                c_end = c_value.length;
            }
            c_value = unescape(c_value.substring(c_start, c_end));
        }
        return c_value;
    },
    getQueryVariable: function(variable) {
        var query = window.location.search.substring(1);
        var vars = query.split("&");
        for (var i = 0; i < vars.length; i++) {
            var pair = vars[i].split("=");
            if (pair[0] == variable) {
                return pair[1];
            }
        }
        return (false);
    },
    goodbye: function(e) {
        if (!e) e = window.event;
        e.cancelBubble = true;
        e.returnValue = 'If you are uploading files, please make sure the operation completes before leaving.';
        if (e.stopPropagation) {
            e.stopPropagation();
            e.preventDefault();
        }
    },
    head: function(img) {
        var result = false;
        $.ajax({
            type: "HEAD",
            async: false,
            url: img,
            success: function(response) {
                result = true;
            },
            failure: function() {
                return false;
            }
        });
        return result;
    },
    loadFile: function(filename, filetype, callback) {
        if (filetype == "js") {
            var fileref = document.createElement('script');
            fileref.setAttribute("type", "text/javascript");
            fileref.setAttribute("src", filename);
        } else if (filetype == "css") {
            var fileref = document.createElement("link");
            fileref.setAttribute("rel", "stylesheet");
            fileref.setAttribute("type", "text/css");
            fileref.setAttribute("href", filename);
        }
        if (typeof fileref != "undefined") document.getElementsByTagName("head")[0].appendChild(fileref);
        if (typeof callback != 'undefined') {
            callback();
        }
    },
    parseStr: function(e, t) {
        var n = String(e).replace(/^&/, "").replace(/&$/, "").split("&"),
            r = n.length,
            i, s, o, u, a, f, l, c, h, p, d, v, m, g, y, b = function(e) {
                return decodeURIComponent(e.replace(/\+/g, "%20"))
            };
        if (!t) {
            t = this.window
        }
        for (i = 0; i < r; i++) {
            p = n[i].split("=");
            d = b(p[0]);
            v = p.length < 2 ? "" : b(p[1]);
            while (d.charAt(0) === " ") {
                d = d.slice(1)
            }
            if (d.indexOf("\0") > -1) {
                d = d.slice(0, d.indexOf("\0"))
            }
            if (d && d.charAt(0) !== "[") {
                g = [];
                m = 0;
                for (s = 0; s < d.length; s++) {
                    if (d.charAt(s) === "[" && !m) {
                        m = s + 1
                    } else if (d.charAt(s) === "]") {
                        if (m) {
                            if (!g.length) {
                                g.push(d.slice(0, m - 1))
                            }
                            g.push(d.substr(m, s - m));
                            m = 0;
                            if (d.charAt(s + 1) !== "[") {
                                break
                            }
                        }
                    }
                }
                if (!g.length) {
                    g = [d]
                }
                for (s = 0; s < g[0].length; s++) {
                    h = g[0].charAt(s);
                    if (h === " " || h === "." || h === "[") {
                        g[0] = g[0].substr(0, s) + "_" + g[0].substr(s + 1)
                    }
                    if (h === "[") {
                        break
                    }
                }
                f = t;
                for (s = 0, y = g.length; s < y; s++) {
                    d = g[s].replace(/^['"]/, "").replace(/['"]$/, "");
                    l = s !== g.length - 1;
                    a = f;
                    if (d !== "" && d !== " " || s === 0) {
                        if (f[d] === c) {
                            f[d] = {}
                        }
                        f = f[d]
                    } else {
                        o = -1;
                        for (u in f) {
                            if (f.hasOwnProperty(u)) {
                                if (+u > o && u.match(/^\d+$/g)) {
                                    o = +u
                                }
                            }
                        }
                        d = o + 1
                    }
                }
                a[d] = v
            }
        }
    },
    populateForm: function(form, obj) {
        $.each(obj, function(key, value) {
            if (typeof value == 'string') {
                $(form).find('[name="' + key + '"]').val(value);
            } else {
                $(form).find('option').each(function() {
                    $(this).prop('selected', false);
                    $(this).removeAttr('selected');
                });
                $.each(value, function(k, v) {
                    $(form + ' option[value="' + v + '"]').prop('selected', true);
                });
            }
        });
    },
    populateMultiText: function(input, data) {
        var name = $(input).attr('name');
        $('[name="' + name + '"]').parents('.quantumLabelF').remove();
        var target = $(input).parents('form:first');
        var c = 0;
        $.each(data, function(l, w) {
            if (c) {
                $(target).find('[name="' + name + '"]').next().find('.quantumLabelFAdder').trigger('click');
            } else {
                $(target).find('[name="' + name + '"]').val(w);
            }
            c++;
        });
        var c = 1;
        $(target).find('.quantumLabelF input[name="' + name + '"]').each(function() {
            $(this).attr('value', data[c]);
            c++;
        });
    },
    processResponse: function(response, s) {
        var suppressErrors = (typeof s == 'undefined' ? false : s);
        var error = '';
        if (response.hasOwnProperty('error')) {
            $.each(response.error, function(key, value) {
                error += value + "\n";
            });
        }
        error = $.trim(error);
        if (error != '') {
            if (error == 'Permission denied.') {
                console.warn(error);
            } else {
                if (suppressErrors) {
                    console.warn(error);
                } else {
                    alert(error);
                }
            }
            return false;
        } else {
            return true;
        }
    },
    saveAs: function(uri, filename) {
        var link = document.createElement('a');
        if (typeof link.download === 'string') {
            document.body.appendChild(link);
            link.download = filename;
            link.href = uri;
            link.click();
            document.body.removeChild(link);
        } else {
            location.replace(uri);
        }
    },
    stripTags: function(str) {
        return String(str).replace(/(<([^>]+)>)/ig, "");
    },
    strPad: function(input, pad_length, pad_string, pad_type) {
        var half = '',
            pad_to_go;
        var str_pad_repeater = function(s, len) {
            var collect = '',
                i;
            while (collect.length < len) {
                collect += s;
            }
            collect = collect.substr(0, len);
            return collect;
        };
        input += '';
        pad_string = pad_string !== undefined ? pad_string : ' ';
        if (pad_type !== 'STR_PAD_LEFT' && pad_type !== 'STR_PAD_RIGHT' && pad_type !== 'STR_PAD_BOTH') {
            pad_type = 'STR_PAD_RIGHT';
        }
        if ((pad_to_go = pad_length - input.length) > 0) {
            if (pad_type === 'STR_PAD_LEFT') {
                input = str_pad_repeater(pad_string, pad_to_go) + input;
            } else if (pad_type === 'STR_PAD_RIGHT') {
                input = input + str_pad_repeater(pad_string, pad_to_go);
            } else if (pad_type === 'STR_PAD_BOTH') {
                half = str_pad_repeater(pad_string, Math.ceil(pad_to_go / 2));
                input = half + input + half;
                input = input.substr(0, pad_length);
            }
        }
        return input;
    },
    toArray: function(obj) {
        var a = [];
        $.each(obj, function(key, value) {
            a.push(value);
        });
        return a;
    },
    toObject: function(arr) {
        var a = {};
        $.each(arr, function(key, value) {
            a[key] = value;
        });
        return a;
    },
    ucwords: function(str) {
        return (str + '').replace(/^([a-z\u00E0-\u00FC])|\s+([a-z\u00E0-\u00FC])/g, function($1) {
            return $1.toUpperCase();
        });
    }
};
var ui = {
    adjustFrame: function(i) {
        if (typeof i == 'undefined') {
            i = 'iframe';
        }
        try {
            var h = $(i).contents().find('body').outerHeight() + 100;
            if (h > 800) {
                $('iframe, .frameContainer').height(h);
            }
        } catch (e) {}
    },
    clearForm: function(form) {
        $(form).find('input').each(function() {
            var type = $(this).attr('type');
            switch (type) {
                case 'submit':
                    break;
                case 'checkbox':
                    $(this).attr('checked', false);
                    break;
                case 'radio':
                    $(this).attr('checked', false);
                    break;
                case 'hidden':
                    break;
                case 'select':
                    break;
                default:
                    $(this).val('');
                    break;
            }
            $(form).find('textarea').each(function() {
                $(this).val('');
            });
            $(form).find('select').each(function() {
                $(this).find('option:first').attr('selected', 'selected');
            });
        });
    },
    delay: function() {
        var timer = 0;
        return function(callback, ms) {
            clearTimeout(timer);
            timer = setTimeout(callback, ms);
        };
    }(),
    displayObject: function(obj) {
        return '<pre> ' + $.trim(String(JSON.stringify(obj, null, 1)).replace(/"/g, '').replace('}', '').replace('{', '').replace(/\r,/g, '')).replace(/,\n/g, "\n") + '</pre>';
    },
    loader: 'R0lGODlh3AATAPQAAP///wAAAL6+vqamppycnLi4uLKyssjIyNjY2MTExNTU1Nzc3ODg4OTk5LCwsLy8vOjo6Ozs7MrKyvLy8vT09M7Ozvb29sbGxtDQ0O7u7tbW1sLCwqqqqvj4+KCgoJaWliH/C05FVFNDQVBFMi4wAwEAAAAh/hpDcmVhdGVkIHdpdGggYWpheGxvYWQuaW5mbwAh+QQJCgAAACwAAAAA3AATAAAF/yAgjmRpnmiqrmzrvnAsz3Rt33iu73zv/8CgcEgECAaEpHLJbDqf0Kh0Sq1ar9isdjoQtAQFg8PwKIMHnLF63N2438f0mv1I2O8buXjvaOPtaHx7fn96goR4hmuId4qDdX95c4+RG4GCBoyAjpmQhZN0YGYFXitdZBIVGAoKoq4CG6Qaswi1CBtkcG6ytrYJubq8vbfAcMK9v7q7D8O1ycrHvsW6zcTKsczNz8HZw9vG3cjTsMIYqQgDLAQGCQoLDA0QCwUHqfYSFw/xEPz88/X38Onr14+Bp4ADCco7eC8hQYMAEe57yNCew4IVBU7EGNDiRn8Z831cGLHhSIgdE/9chIeBgDoB7gjaWUWTlYAFE3LqzDCTlc9WOHfm7PkTqNCh54rePDqB6M+lR536hCpUqs2gVZM+xbrTqtGoWqdy1emValeXKwgcWABB5y1acFNZmEvXwoJ2cGfJrTv3bl69Ffj2xZt3L1+/fw3XRVw4sGDGcR0fJhxZsF3KtBTThZxZ8mLMgC3fRatCLYMIFCzwLEprg84OsDus/tvqdezZf13Hvr2B9Szdu2X3pg18N+68xXn7rh1c+PLksI/Dhe6cuO3ow3NfV92bdArTqC2Ebc3A8vjf5QWf15Bg7Nz17c2fj69+fnq+8N2Lty+fuP78/eV2X13neIcCeBRwxorbZrAxAJoCDHbgoG8RTshahQ9iSKEEzUmYIYfNWViUhheCGJyIP5E4oom7WWjgCeBBAJNv1DVV01MZdJhhjdkplWNzO/5oXI846njjVEIqR2OS2B1pE5PVscajkxhMycqLJgxQCwT40PjfAV4GqNSXYdZXJn5gSkmmmmJu1aZYb14V51do+pTOCmA00AqVB4hG5IJ9PvYnhIFOxmdqhpaI6GeHCtpooisuutmg+Eg62KOMKuqoTaXgicQWoIYq6qiklmoqFV0UoeqqrLbq6quwxirrrLTWauutJ4QAACH5BAkKAAAALAAAAADcABMAAAX/ICCOZGmeaKqubOu+cCzPdG3feK7vfO//wKBwSAQIBoSkcslsOp/QqHRKrVqv2Kx2OhC0BAXHx/EoCzboAcdhcLDdgwJ6nua03YZ8PMFPoBMca215eg98G36IgYNvDgOGh4lqjHd7fXOTjYV9nItvhJaIfYF4jXuIf4CCbHmOBZySdoOtj5eja59wBmYFXitdHhwSFRgKxhobBgUPAmdoyxoI0tPJaM5+u9PaCQZzZ9gP2tPcdM7L4tLVznPn6OQb18nh6NV0fu3i5OvP8/nd1qjwaasHcIPAcf/gBSyAAMMwBANYEAhWYQGDBhAyLihwYJiEjx8fYMxIcsGDAxVA/yYIOZIkBAaGPIK8INJlRpgrPeasaRPmx5QgJfB0abLjz50tSeIM+pFmUo0nQQIV+vRlTJUSnNq0KlXCSq09ozIFexEBAYkeNiwgOaEtn2LFpGEQsKCtXbcSjOmVlqDuhAx3+eg1Jo3u37sZBA9GoMAw4MB5FyMwfLht4sh7G/utPGHlYAV8Nz9OnOBz4c2VFWem/Pivar0aKCP2LFn2XwhnVxBwsPbuBAQbEGiIFg1BggoWkidva5z4cL7IlStfkED48OIYoiufYIH68+cKPkqfnsB58ePjmZd3Dj199/XE20tv6/27XO3S6z9nPCz9BP3FISDefL/Bt192/uWmAv8BFzAQAQUWWFaaBgqA11hbHWTIXWIVXifNhRlq6FqF1sm1QQYhdiAhbNEYc2KKK1pXnAIvhrjhBh0KxxiINlqQAY4UXjdcjSJyeAx2G2BYJJD7NZQkjCPKuCORKnbAIXsuKhlhBxEomAIBBzgIYXIfHfmhAAyMR2ZkHk62gJoWlNlhi33ZJZ2cQiKTJoG05Wjcm3xith9dcOK5X51tLRenoHTuud2iMnaolp3KGXrdBo7eKYF5p/mXgJcogClmcgzAR5gCKymXYqlCgmacdhp2UCqL96mq4nuDBTmgBasaCFp4sHaQHHUsGvNRiiGyep1exyIra2mS7dprrtA5++z/Z8ZKYGuGsy6GqgTIDvupRGE+6CO0x3xI5Y2mOTkBjD4ySeGU79o44mcaSEClhglgsKyJ9S5ZTGY0Bnzrj+3SiKK9Rh5zjAALCywZBk/ayCWO3hYM5Y8Dn6qxxRFsgAGoJwwgDQRtYXAAragyQOmaLKNZKGaEuUlpyiub+ad/KtPqpntypvvnzR30DBtjMhNodK6Eqrl0zU0/GjTUgG43wdN6Ra2pAhGtAAZGE5Ta8TH6wknd2IytNKaiZ+Or79oR/tcvthIcAPe7DGAs9Edwk6r3qWoTaNzY2fb9HuHh2S343Hs1VIHhYtOt+Hh551rh24vP5YvXSGzh+eeghy76GuikU9FFEainrvrqrLfu+uuwxy777LTXfkIIACH5BAkKAAAALAAAAADcABMAAAX/ICCOZGmeaKqubOu+cCzPdG3feK7vfO//wKBwSAQIBoSkcslsOp/QqHRKrVqv2Kx2OhC0BAWHB2l4CDZo9IDjcBja7UEhTV+3DXi3PJFA8xMcbHiDBgMPG31pgHBvg4Z9iYiBjYx7kWocb26OD398mI2EhoiegJlud4UFiZ5sm6Kdn2mBr5t7pJ9rlG0cHg5gXitdaxwFGArIGgoaGwYCZ3QFDwjU1AoIzdCQzdPV1c0bZ9vS3tUJBmjQaGXl1OB0feze1+faiBvk8wjnimn55e/o4OtWjp+4NPIKogsXjaA3g/fiGZBQAcEAFgQGOChgYEEDCCBBLihwQILJkxIe/3wMKfJBSQkJYJpUyRIkgwcVUJq8QLPmTYoyY6ZcyfJmTp08iYZc8MBkhZgxk9aEcPOlzp5FmwI9KdWn1qASurJkClRoWKwhq6IUqpJBAwQEMBYroAHkhLt3+RyzhgCDgAV48Wbgg+waAnoLMgTOm6DwQ8CLBzdGdvjw38V5JTg2lzhyTMeUEwBWHPgzZc4TSOM1bZia6LuqJxCmnOxv7NSsl1mGHHiw5tOuIWeAEHcFATwJME/ApgFBc3MVLEgPvE+Ddb4JokufPmFBAuvPXWu3MIF89wTOmxvOvp179evQtwf2nr6aApPyzVd3jn089e/8xdfeXe/xdZ9/d1ngHf98lbHH3V0LMrgPgsWpcFwBEFBgHmyNXWeYAgLc1UF5sG2wTHjIhNjBiIKZCN81GGyQwYq9uajeMiBOQGOLJ1KjTI40kmfBYNfc2NcGIpI4pI0vyrhjiT1WFqOOLEIZnjVOVpmajYfBiCSNLGbA5YdOkjdihSkQwIEEEWg4nQUmvYhYe+bFKaFodN5lp3rKvJYfnBKAJ+gGDMi3mmbwWYfng7IheuWihu5p32XcSWdSj+stkF95dp64jJ+RBipocHkCCp6PCiRQ6INookCAAwy0yd2CtNET3Yo7RvihBjFZAOaKDHT43DL4BQnsZMo8xx6uI1oQrHXXhHZrB28G62n/YSYxi+uzP2IrgbbHbiaer7hCiOxDFWhrbmGnLVuus5NFexhFuHLX6gkEECorlLpZo0CWJG4pLjIACykmBsp0eSSVeC15TDJeUhlkowlL+SWLNJpW2WEF87urXzNWSZ6JOEb7b8g1brZMjCg3ezBtWKKc4MvyEtwybPeaMAA1ECRoAQYHYLpbeYYCLfQ+mtL5c9CnfQpYpUtHOSejEgT9ogZ/GSqd0f2m+LR5WzOtHqlQX1pYwpC+WbXKqSYtpJ5Mt4a01lGzS3akF60AxkcTaLgAyRBPWCoDgHfJqwRuBuzdw/1ml3iCwTIeLUWJN0v4McMe7uasCTxseNWPSxc5RbvIgD7geZLbGrqCG3jepUmbbze63Y6fvjiOylbwOITPfIHEFsAHL/zwxBdvPBVdFKH88sw37/zz0Ecv/fTUV2/99SeEAAAh+QQJCgAAACwAAAAA3AATAAAF/yAgjmRpnmiqrmzrvnAsz3Rt33iu73zv/8CgcEgECAaEpHLJbDqf0Kh0Sq1ar9isdjoQtAQFh2cw8BQEm3T6yHEYHHD4oKCuD9qGvNsxT6QTgAkcHHmFeX11fm17hXwPG35qgnhxbwMPkXaLhgZ9gWp3bpyegX4DcG+inY+Qn6eclpiZkHh6epetgLSUcBxlD2csXXdvBQrHGgoaGhsGaIkFDwjTCArTzX+QadHU3c1ofpHc3dcGG89/4+TYktvS1NYI7OHu3fEJ5tpqBu/k+HX7+nXDB06SuoHm0KXhR65cQT8P3FRAMIAFgVMPwDCAwLHjggIHJIgceeFBg44eC/+ITCCBZYKSJ1FCWPBgpE2YMmc+qNCypwScMmnaXAkUJYOaFVyKLOqx5tCXJnMelcBzJNSYKIX2ZPkzqsyjPLku9Zr1QciVErYxaICAgEUOBRJIgzChbt0MLOPFwyBggV27eCUcmxZvg9+/dfPGo5bg8N/Ag61ZM4w4seDF1fpWhizZmoa+GSortgcaMWd/fkP/HY0MgWbTipVV++wY8GhvqSG4XUEgoYTKE+Qh0OCvggULiBckWEZ4Ggbjx5HXVc58IPQJ0idQJ66XanTpFraTe348+XLizRNcz658eHMN3rNPT+C+G/nodqk3t6a+fN3j+u0Xn3nVTQPfdRPspkL/b+dEIN8EeMm2GAYbTNABdrbJ1hyFFv5lQYTodSZABhc+loCEyhxTYYkZopdMMiNeiBxyIFajV4wYHpfBBspUl8yKHu6ooV5APsZjQxyyeNeJ3N1IYod38cgdPBUid6GCKfRWgAYU4IccSyHew8B3doGJHmMLkGkZcynKk2Z50Ym0zJzLbDCmfBbI6eIyCdyJmJmoqZmnBAXy9+Z/yOlZDZpwYihnj7IZpuYEevrYJ5mJEuqiof4l+NYDEXQpXQcMnNjZNDx1oGqJ4S2nF3EsqWrhqqVWl6JIslpAK5MaIqDeqjJq56qN1aTaQaPbHTPYr8Be6Gsyyh6Da7OkmmqP/7GyztdrNVQBm5+pgw3X7aoYKhfZosb6hyUKBHCgQKij1rghkOAJuZg1SeYIIY+nIpDvf/sqm4yNG5CY64f87qdAwSXKGqFkhPH1ZHb2EgYtw3bpKGVkPz5pJAav+gukjB1UHE/HLNJobWcSX8jiuicMMBFd2OmKwQFs2tjXpDfnPE1j30V3c7iRHlrzBD2HONzODyZtsQJMI4r0AUNaE3XNHQw95c9GC001MpIxDacFQ+ulTNTZlU3O1eWVHa6vb/pnQUUrgHHSBKIuwG+bCPyEqbAg25gMVV1iOB/IGh5YOKLKIQ6xBAcUHmzjIcIqgajZ+Ro42DcvXl7j0U4WOUd+2IGu7DWjI1pt4DYq8BPm0entuGSQY/4tBi9Ss0HqfwngBQtHbCH88MQXb/zxyFfRRRHMN+/889BHL/301Fdv/fXYZ39CCAAh+QQJCgAAACwAAAAA3AATAAAF/yAgjmRpnmiqrmzrvnAsz3Rt33iu73zv/8CgcEgECAaEpHLJbDqf0Kh0Sq1ar9isdjoQtAQFh2fAKXsKm7R6Q+Y43vABep0mGwwOPH7w2CT+gHZ3d3lyagl+CQNvg4yGh36LcHoGfHR/ZYOElQ9/a4ocmoRygIiRk5p8pYmZjXePaYBujHoOqp5qZHBlHAUFXitddg8PBg8KGsgayxvGkAkFDwgICtPTzX2mftHW3QnOpojG3dbYkNjk1waxsdDS1N7ga9zw1t/aifTk35fu6Qj3numL14fOuHTNECHqU4DDgQEsCCwidiHBAwYQMmpcUOCAhI8gJVzUuLGThAQnP/9abEAyI4MCIVOKZNnyJUqUJxNcGNlywYOQgHZirGkSJ8gHNEky+AkS58qWEJYC/bMzacmbQHkqNdlUJ1KoSz2i9COhmQYCEXtVrCBgwYS3cCf8qTcNQ9u4cFFOq2bPLV65Cf7dxZthbjW+CgbjnWtNgWPFcAsHdoxgWWK/iyV045sAc2S96SDn1exYw17REwpLQEYt2eW/qtPZRQAB7QoC61RW+GsBwYZ/CXb/XRCYLsAKFizEtUAc+G7lcZsjroscOvTmsoUvx15PwccJ0N8yL17N9PG/E7jv9S4hOV7pdIPDdZ+ePDzv2qMXn2b5+wTbKuAWnF3oZbABZY0lVmD/ApQd9thybxno2GGuCVDggaUpoyBsB1bGGgIYbJCBcuFJiOAyGohIInQSmmdeiBnMF2GHfNUlIoc1rncjYRjW6NgGf3VQGILWwNjBfxEZcAFbC7gHXQcfUYOYdwzQNxo5yUhQZXhvRYlMeVSuSOJHKJa5AQMQThBlZWZ6Bp4Fa1qzTAJbijcBlJrtxeaZ4lnnpZwpukWieGQmYx5ATXIplwTL8DdNZ07CtWYybNIJF4Ap4NZHe0920AEDk035kafieQrqXofK5ympn5JHKYjPrfoWcR8WWQGp4Ul32KPVgXdnqxM6OKqspjIYrGPDrlrsZtRIcOuR86nHFwbPvmes/6PH4frrqbvySh+mKGhaAARPzjjdhCramdoGGOhp44i+zogBkSDuWC5KlE4r4pHJkarXrj++Raq5iLmWLlxHBteavjG+6amJrUkJJI4Ro5sBv9AaOK+jAau77sbH7nspCwNIYIACffL7J4JtWQnen421nNzMcB6AqpRa9klonmBSiR4GNi+cJZpvwgX0ejj71W9yR+eIgaVvQgf0l/A8nWjUFhwtZYWC4hVnkZ3p/PJqNQ5NnwUQrQCGBBBMQIGTtL7abK+5JjAv1fi9bS0GLlJHgdjEgYzzARTwC1fgEWdJuKKBZzj331Y23qB3i9v5aY/rSUC4w7PaLeWXmr9NszMFoN79eeiM232o33EJAIzaSGwh++y012777bhT0UURvPfu++/ABy/88MQXb/zxyCd/QggAIfkECQoAAAAsAAAAANwAEwAABf8gII5kaZ5oqq5s675wLM90bd94ru987//AoHBIBAgGhKRyyWw6n9CodEqtWq/YrHY6ELQEBY5nwCk7xIWNer0hO95wziC9Ttg5b4ND/+Y87IBqZAaEe29zGwmJigmDfHoGiImTjXiQhJEPdYyWhXwDmpuVmHwOoHZqjI6kZ3+MqhyemJKAdo6Ge3OKbEd4ZRwFBV4rc4MPrgYPChrMzAgbyZSJBcoI1tfQoYsJydfe2amT3d7W0OGp1OTl0YtqyQrq0Lt11PDk3KGoG+nxBpvTD9QhwCctm0BzbOyMIwdOUwEDEgawIOCB2oMLgB4wgMCx44IHBySIHClBY0ePfyT/JCB5weRJCAwejFw58kGDlzBTqqTZcuPLmCIBiWx58+VHmiRLFj0JVCVLl0xl7qSZwCbOo0lFWv0pdefQrVFDJtr5gMBEYBgxqBWwYILbtxPsqMPAFu7blfa81bUbN4HAvXAzyLWnoDBguHIRFF6m4LBbwQngMYPXuC3fldbyPrMcGLM3w5wRS1iWWUNlvnElKDZtz/EEwaqvYahQoexEfyILi4RrYYKFZwJ3810QWZ2ECrx9Ew+O3K6F5Yq9zXbb+y30a7olJJ+wnLC16W97Py+uwdtx1NcLWzs/3G9e07stVPc9kHJ0BcLtQp+c3ewKAgYkUAFpCaAmmHqKLSYA/18WHEiZPRhsQF1nlLFWmIR8ZbDBYs0YZuCGpGXWmG92aWiPMwhEOOEEHXRwIALlwXjhio+BeE15IzpnInaLbZBBhhti9x2GbnVQo2Y9ZuCfCgBeMCB+DJDIolt4iVhOaNSJdCOBUfIlkmkyMpPAAvKJ59aXzTQzJo0WoJnmQF36Jp6W1qC4gWW9GZladCiyJd+KnsHImgRRVjfnaDEKuiZvbcYWo5htzefbl5LFWNeSKQAo1QXasdhiiwwUl2B21H3aQaghXnPcp1NagCqYslXAqnV+zYWcpNwVp9l5eepJnHqL4SdBi56CGlmw2Zn6aaiZjZqfb8Y2m+Cz1O0n3f+tnvrGbF6kToApCgAWoNWPeh754JA0vmajiAr4iOuOW7abQXVGNriBWoRdOK8FxNqLwX3oluubhv8yluRbegqGb536ykesuoXhyJqPQJIGbLvQhkcwjKs1zBvBwSZIsbcsDCCBAAf4ya+UEhyQoIiEJtfoZ7oxUOafE2BwgMWMqUydfC1LVtiArk0QtGkWEopzlqM9aJrKHfw5c6wKjFkmXDrbhwFockodtMGFLWpXy9JdiXN1ZDNszV4WSLQCGBKoQYHUyonqrHa4ErewAgMmcAAF7f2baIoVzC2p3gUvJtLcvIWqloy6/R04mIpLwDhciI8qLOB5yud44pHPLbA83hFDWPjNbuk9KnySN57Av+TMBvgEAgzzNhJb5K777rz37vvvVHRRxPDEF2/88cgnr/zyzDfv/PPQnxACACH5BAkKAAAALAAAAADcABMAAAX/ICCOZGmeaKqubOu+cCzPdG3feK7vfO//wKBwSAQIBoSkcslsOp/QqHRKrVqv2Kx2OhC0BIUCwcMpO84OT2HDbm8GHLQjnn6wE3g83SA3DB55G3llfHxnfnZ4gglvew6Gf4ySgmYGlpCJknochWiId3kJcZZyDn93i6KPl4eniopwq6SIoZKxhpenbhtHZRxhXisDopwPgHkGDxrLGgjLG8mC0gkFDwjX2AgJ0bXJ2djbgNJsAtbfCNB2oOnn6MmKbeXt226K1fMGi6j359D69ua+QZskjd+3cOvY9XNgp4ABCQNYEDBl7EIeCQkeMIDAseOCBwckiBSZ4ILGjh4B/40kaXIjSggMHmBcifHky5gYE6zM2OAlzGM6Z5rs+fIjTZ0tfcYMSlLCUJ8fL47kCVXmTjwPiKJkUCDnyqc3CxzQmYeAxAEGLGJYiwCDgAUT4sqdgOebArdw507IUNfuW71xdZ7DC5iuhGsKErf9CxhPYgUaEhPWyzfBMgUIJDPW6zhb5M1y+R5GjFkBaLmCM0dOfHqvztXYJnMejaFCBQlmVxAYsEGkYnQV4lqYMNyCtnYSggNekAC58uJxmTufW5w55mwKkg+nLp105uTC53a/nhg88fMTmDfDVl65Xum/IZt/3/zaag3a5W63nll1dvfiWbaaZLmpQIABCVQA2f9lAhTG112PQWYadXE9+FtmEwKWwQYQJrZagxomsOCAGVImInsSbpCBhhwug6KKcXXQQYUcYuDMggrASFmNzjjzzIrh7cUhhhHqONeGpSEW2QYxHsmjhxpgUGAKB16g4IIbMNCkXMlhaJ8GWVJo2I3NyKclYF1GxgyYDEAnXHJrMpNAm/rFBSczPiYAlwXF8ZnmesvoOdyMbx7m4o0S5LWdn4bex2Z4xYmEzaEb5EUcnxbA+WWglqIn6aHPTInCgVbdlZyMqMrIQHMRSiaBBakS1903p04w434n0loBoQFOt1yu2YAnY68RXiNsqh2s2qqxuyKb7Imtmgcrqsp6h8D/fMSpapldx55nwayK/SfqCQd2hcFdAgDp5GMvqhvakF4mZuS710WGIYy30khekRkMu92GNu6bo7r/ttjqwLaua5+HOdrKq5Cl3dcwi+xKiLBwwwom4b0E6xvuYyqOa8IAEghwQAV45VvovpkxBl2mo0W7AKbCZXoAhgMmWnOkEqx2JX5nUufbgJHpXCfMOGu2QAd8eitpW1eaNrNeMGN27mNz0swziYnpSbXN19gYtstzfXrdYjNHtAIYGFVwwAEvR1dfxdjKxVzAP0twAAW/ir2w3nzTd3W4yQWO3t0DfleB4XYnEHCEhffdKgaA29p0eo4fHLng9qoG+OVyXz0gMeWGY7qq3xhiRIEAwayNxBawxy777LTXbjsVXRSh++689+7778AHL/zwxBdv/PEnhAAAIfkECQoAAAAsAAAAANwAEwAABf8gII5kaZ5oqq5s675wLM90bd94ru987//AoHBIBAgGhKRyyWw6n9CodEqtWq/YrHY6ELQEhYLD4BlwHGg0ubBpuzdm9Dk9eCTu+MTZkDb4PXYbeIIcHHxqf4F3gnqGY2kOdQmCjHCGfpCSjHhmh2N+knmEkJmKg3uHfgaaeY2qn6t2i4t7sKAPbwIJD2VhXisDCQZgDrKDBQ8aGgjKyhvDlJMJyAjV1gjCunkP1NfVwpRtk93e2ZVt5NfCk27jD97f0LPP7/Dr4pTp1veLgvrx7AL+Q/BM25uBegoYkDCABYFhEobhkUBRwoMGEDJqXPDgQMUEFC9c1LjxQUUJICX/iMRIEgIDkycrjmzJMSXFlDNJvkwJsmdOjQwKfDz5M+PLoSGLQqgZU6XSoB/voHxawGbFlS2XGktAwKEADB0xiEWAodqGBRPSqp1wx5qCamDRrp2Qoa3bagLkzrULF4GCvHPTglRAmKxZvWsHayBcliDitHUlvGWM97FgCdYWVw4c2e/kw4HZJlCwmDBhwHPrjraGYTHqtaoxVKggoesKAgd2SX5rbUMFCxOAC8cGDwHFwBYWJCgu4XfwtcqZV0grPHj0u2SnqwU+IXph3rK5b1fOu7Bx5+K7L6/2/Xhg8uyXnQ8dvfRiDe7TwyfNuzlybKYpgIFtKhAgwEKkKcOf/wChZbBBgMucRh1so5XH3wbI1WXafRJy9iCErmX4IWHNaIAhZ6uxBxeGHXQA24P3yYfBBhmgSBozESpwongWOBhggn/N1aKG8a1YY2oVAklgCgQUUwGJ8iXAgItrWUARbwpqIOWEal0ZoYJbzmWlZCWSlsAC6VkwZonNbMAAl5cpg+NiZwpnJ0Xylegmlc+tWY1mjnGnZnB4QukMA9UJRxGOf5r4ppqDjjmnfKilh2ejGiyJAgF1XNmYbC2GmhZ5AcJVgajcXecNqM9Rx8B6bingnlotviqdkB3YCg+rtOaapFsUhSrsq6axJ6sEwoZK7I/HWpCsr57FBxJ1w8LqV/81zbkoXK3LfVeNpic0KRQG4NHoIW/XEmZuaiN6tti62/moWbk18uhjqerWS6GFpe2YVotskVssWfBOAHACrZHoWcGQwQhlvmsdXBZ/F9YLMF2jzUuYBP4a7CLCnoEHrgkDSCDAARUILAGaVVqAwQHR8pZXomm9/ONhgjrbgc2lyYxmpIRK9uSNjrXs8gEbTrYyl2ryTJmsLCdKkWzFQl1lWlOXGmifal6p9VnbQfpyY2SZyXKVV7JmZkMrgIFSyrIeUJ2r7YKnXdivUg1kAgdQ8B7IzJjGsd9zKSdwyBL03WpwDGxwuOASEP5vriO2F3nLjQdIrpaRDxqcBdgIHGA74pKrZXiR2ZWuZt49m+o3pKMC3p4Av7SNxBa456777rz37jsVXRQh/PDEF2/88cgnr/zyzDfv/PMnhAAAIfkECQoAAAAsAAAAANwAEwAABf8gII5kaZ5oqq5s675wLM90bd94ru987//AoHBIBAgGhKRyyWw6n9CodEqtWq/YrHY6ELQEhYLDUPAMHGi0weEpbN7wI8cxTzsGj4R+n+DUxwaBeBt7hH1/gYIPhox+Y3Z3iwmGk36BkIN8egOIl3h8hBuOkAaZhQlna4BrpnyWa4mleZOFjrGKcXoFA2ReKwMJBgISDw6abwUPGggazc0bBqG0G8kI1tcIwZp51djW2nC03d7BjG8J49jl4cgP3t/RetLp1+vT6O7v5fKhAvnk0UKFogeP3zmCCIoZkDCABQFhChQYuKBHgkUJkxpA2MhxQYEDFhNcvPBAI8eNCx7/gMQYckPJkxsZPLhIM8FLmDJrYiRp8mTKkCwT8IQJwSPQkENhpgQpEunNkzlpWkwKdSbGihKocowqVSvKWQkIOBSgQOYFDBgQpI0oYMGEt3AzTLKm4BqGtnDjirxW95vbvG/nWlub8G9euRsiqqWLF/AEkRoiprX2wLDeDQgkW9PQGLDgyNc665WguK8C0XAnRY6oGPUEuRLsgk5g+a3cCxUqSBC7gsCBBXcVq6swwULx4hayvctGPK8FCwsSLE9A3Hje6NOrHzeOnW695sffRi/9HfDz7sIVSNB+XXrmugo0rHcM3X388o6jr44ceb51uNjF1xcC8zk3wXiS8aYC/wESaLABBs7ch0ECjr2WAGvLsLZBeHqVFl9kGxooV0T81TVhBo6NiOEyJ4p4IYnNRBQiYCN6x4wCG3ZAY2If8jXjYRcyk2FmG/5nXAY8wqhWAii+1YGOSGLoY4VRfqiAgikwmIeS1gjAgHkWYLQZf9m49V9gDWYWY5nmTYCRM2TS5pxxb8IZGV5nhplmhJyZadxzbrpnZ2d/6rnZgHIid5xIMDaDgJfbLdrgMkKW+Rygz1kEZz1mehabkBpgiQIByVikwGTqVfDkk2/Vxxqiqur4X3fksHccre8xlxerDLiHjQIVUAgXr77yFeyuOvYqXGbMrbrqBMqaFpFFzhL7qv9i1FX7ZLR0LUNdcc4e6Cus263KbV+inkAAHhJg0BeITR6WmHcaxhvXg/AJiKO9R77ILF1FwmVdAu6WBu+ZFua72mkZWMfqBElKu0G8rFZ5n4ATp5jkmvsOq+Nj7u63ZMMPv4bveyYy6fDH+C6brgnACHBABQUrkGirz2FwAHnM4Mmhzq9yijOrOi/MKabH6VwBiYwZdukEQAvILKTWXVq0ZvH5/CfUM7M29Zetthp1eht0eqkFYw8IKXKA6mzXfTeH7fZg9zW0AhgY0TwthUa6Ch9dBeIsbsFrYkRBfgTfiG0FhwMWnbsoq3cABUYOnu/ejU/A6uNeT8u4wMb1WnBCyJJTLjjnr8o3OeJrUcpc5oCiPqAEkz8tXuLkPeDL3Uhs4fvvwAcv/PDEU9FFEcgnr/zyzDfv/PPQRy/99NRXf0IIACH5BAkKAAAALAAAAADcABMAAAX/ICCOZGmeaKqubOu+cCzPdG3feK7vfO//wKBwSAQIBoSkcslsOp/QqHRKrVqv2Kx2OhC0BIWCw/AoDziOtCHt8BQ28PjmzK57Hom8fo42+P8DeAkbeYQcfX9+gYOFg4d1bIGEjQmPbICClI9/YwaLjHAJdJeKmZOViGtpn3qOqZineoeJgG8CeWUbBV4rAwkGAhIVGL97hGACGsrKCAgbBoTRhLvN1c3PepnU1s2/oZO6AtzdBoPf4eMI3tIJyOnF0YwFD+nY8e3z7+Xfefnj9uz8cVsXCh89axgk7BrAggAwBQsYIChwQILFixIeNIDAseOCBwcSXMy2sSPHjxJE/6a0eEGjSY4MQGK86PIlypUJEmYsaTKmyJ8JW/Ls6HMkzaEn8YwMWtPkx4pGd76E4DMPRqFTY860OGhogwYagBFoKEABA46DEGBAoEBB0AUT4sqdIFKBNbcC4M6dkEEk22oYFOTdG9fvWrtsBxM23MytYL17666t9phwXwlum2lIDHmuSA2IGyuOLOHv38qLMbdFjHruZbWgRXeOe1nC2BUEDiyAMMHZuwoTLAQX3nvDOAUW5Vogru434d4JnAsnPmFB9NBshQXfa9104+Rxl8e13rZxN+CEydtVsFkd+vDjE7C/q52wOvb4s7+faz025frbxefWbSoQIAEDEUCwgf9j7bUlwHN9ZVaegxDK1xYzFMJH24L5saXABhlYxiEzHoKoIV8LYqAMaw9aZqFmJUK4YHuNfRjiXhmk+NcyJgaIolvM8BhiBx3IleN8lH1IWAcRgkZgCgYiaBGJojGgHHFTgtagAFYSZhF7/qnTpY+faVlNAnqJN0EHWa6ozAZjBtgmmBokwMB01LW5jAZwbqfmlNips4B4eOqJgDJ2+imXRZpthuigeC6XZTWIxilXmRo8iYKBCwiWmWkJVEAkfB0w8KI1IvlIpKnOkVpqdB5+h96o8d3lFnijrgprjbfGRSt0lH0nAZG5vsprWxYRW6Suq4UWqrLEsspWg8Io6yv/q6EhK0Fw0GLbjKYn5CZYBYht1laPrnEY67kyrhYbuyceiR28Pso7bYwiXjihjWsWuWF5p/H765HmNoiur3RJsGKNG/jq748XMrwmjhwCfO6QD9v7LQsDxPTAMKsFpthyJCdkmgYiw0VdXF/Om9dyv7YMWGXTLYpZg5wNR11C78oW3p8HSGgul4qyrJppgllJHJZHn0Y0yUwDXCXUNquFZNLKyYXBAVZvxtAKYIQEsmPgDacr0tltO1y/DMwYpkgUpJfTasLGzd3cdCN3gN3UWRcY3epIEPevfq+3njBxq/kqBoGBduvea8f393zICS63ivRBTqgFpgaWZEIUULdcK+frIfAAL2AjscXqrLfu+uuwx05FF0XUbvvtuOeu++689+7778AHL/wJIQAAOwAAAAAAAAAAAA==',
    loading: function(t, done) {
        var templateUrl = (typeof config == 'undefined' ? parent.config.templateUrl : config.templateUrl);
        if (typeof done == 'undefined') {
            done = false;
        }
        if (done) {
            $(t).find('*').css('visibility', 'visible');
            $(t).css({
                'background-image': 'none',
                'min-height': 0
            });
        } else {
            var img = (typeof templateUrl == 'undefined' ? 'url(data:image/gif;base64,' + ui.loader + ')' : 'url(' + templateUrl + 'img/ajax-loader.gif)');
            $(t).find('*').css('visibility', 'hidden');
            $(t).css({
                'background-image': img,
                'background-repeat': 'no-repeat',
                'background-position': 'center center',
                'min-height': 500
            });
        }
    }
};

function Form(inputObj, callback) {
    this.ac = function(key, data) {
        var info = []
        $.each(data, function(k, v) {
            info.push(v);
        });
        var that = this;
        $(that.target).find('[name="' + key + '"]').autocomplete({
            source: info,
            minLength: 0
        });
    }
    this.create = function() {
        var that = this;
        var d = new Date();
        var ac = []
        $(this.options.target).html('');
        this.id = '__' + this.options.name + d.getTime();
        var html = '<form data-type="' + this.options.type + '" enctype="multipart/form-data" id="' + this.id + '">';
        $.each(this.formData, function(key, value) {
            var myField = new Field(value);
            if (myField.atts.autocomplete == 'yes') {
                ac.push({
                    'field': myField.atts.name,
                    'type': that.options.type,
                    'id': myField.id
                });
            }
            html += myField.output;
        });
        html += '<button class="btn btn-primary" data-loading-text="Please Wait" autocomplete="off">Submit</button></form>';
        $(document).off('submit', '#' + this.id);
        if (this.options.event) {
            Submit('#' + this.id, this.options.event);
        } else {
            Submit('#' + this.id, this.event);
        }
        ui.loading(this.options.target, true);
        $(this.options.target).html(html);
        $('.btn').button();
        if (this.options.setup) {
            $.each(this.options.setup, function(key, value) {
                var html = '';
                $.each(value, function(k, v) {
                    html += '<option value="' + k + '">' + v + '</option>';
                });
                $('#' + that.id).find('[name="' + key + '"]').html(html);
            });
        }
        if (ac.length) {
            $.each(ac, function(key, value) {
                $(document).on('keyup', '[list="' + value.id + '"]', function() {
                    var that = this;
                    ui.delay(function() {
                        var request = new Request({
                            control: {
                                'action': 'terms',
                                'term': $(that).val(),
                                'field': value.field,
                                'type': value.type
                            }
                        });
                        api.call(request.output(), function(response) {
                            $('#' + value.id).html('');
                            $.each(response.output, function(k, v) {
                                $('#' + value.id).append('<option value="' + v + '"></option>');
                            });
                        });
                    }, 250);
                });
            });
        }
        if (this.options.data) {
            var populate = setInterval(function() {
                if ($(that.options.target).find('form').length) {
                    clearInterval(populate);
                    that.populate(that.options.data);
                }
            }, 250);
        }
        if (typeof callback != 'undefined') {
            callback(this);
        }
    }
    this.event = function(t, e) {
        var r = {
            action: 'post',
            type: $(t).attr('data-type'),
            id: ''
        };
        var d = ['data-action', 'data-type', 'data-id'];
        var that = this;
        $.each(d, function(key, value) {
            var v = $(t).attr(value);
            if (typeof v != 'undefined') {
                if (v) {
                    r[value.replace('data-', '')] = v;
                }
            }
        });
        var btn = $(t).find('button.btn-primary');
        if (that.bootstrap) {
            $(btn).button('loading');
        }
        var request = new Request({
            control: r,
            data: $(t).serializeObject()
        });
        api.call(request.output(), function(response) {
            alert('Success');
            if (that.bootstrap) {
                $(btn).button('reset');
            }
            ui.clearForm(t);
            $(t).trigger('formSuccess', request.data);
        }, function() {
            if (that.bootstrap) {
                $(btn).button('reset');
            }
            $(t).trigger('formError', request.data);
        });
    }
    this.get = function() {
        if (typeof this.options.form == 'object') {
            this.formData = this.options.form;
            this.create();
        } else {
            var that = this;
            var control = {
                action: 'get',
                type: 'form',
                id: that.options.name
            }
            if (that.options.app) control.app = that.options.app;
            var request = new Request(control);
            api.call(request.output(), function(response) {
                if (typeof response.output != 'undefined') {
                    if (typeof response.output.form != 'undefined') {
                        that.formData = response.output.form;
                        that.create();
                    }
                }
            }, function() {
                alert('Form not found, please contact the administrator.');
                ui.loading(this.options.target, true);
            });
        }
    }
    this.init = function() {
        ui.loading(this.options.target);
        this.get();
    }
    this.populate = function(obj) {
        var that = this;
        if (typeof obj != 'undefined') {
            $.each(obj, function(k, v) {
                if (v) {
                    switch (typeof v) {
                        case 'object':
                        case 'array':
                            var w = library.toArray(v);
                            var n1 = $(that.options.target + ' [name="' + k + '[]"]').length;
                            var n2 = $(that.options.target + ' [name="' + k + '"]').length;
                            var tag = 'input';
                            if (n1) {
                                tag = String($(that.options.target + ' [name="' + k + '[]"]').get(0).tagName).toLowerCase();
                            }
                            if (n2) {
                                tag = String($(that.options.target + ' [name="' + k + '"]').get(0).tagName).toLowerCase();
                            }
                            if (tag == 'select') {
                                var name = (n1 ? '[name="' + k + '[]"]' : '[name="' + k + '"]');
                                $(that.options.target).find(name + ' option').removeAttr('selected');
                                $.each(w, function(myKey, myVal) {
                                    $(that.options.target + ' [value="' + myVal + '"]').attr('selected', true);
                                });
                            } else {
                                var c = 0;
                                $.each(v, function(l, w) {
                                    if (c) {
                                        $(that.options.target).find('[name="' + k + '[]"]').next().find('.quantumLabelFAdder').trigger('click');
                                    } else {
                                        $(that.options.target).find('[name="' + k + '[]"]').val(w);
                                    }
                                    c++;
                                });
                                var c = 1;
                                $(that.options.target).find('.quantumLabelF input[name="' + k + '[]"]').each(function() {
                                    $(this).attr('value', v[c]);
                                    c++;
                                });
                            }
                            break;
                        default:
                            var type = $(that.options.target).find('[name="' + k + '"]').attr('type');
                            if (typeof type == 'undefined') {
                                $(that.options.target).find('[name="' + k + '"]').val(v);
                            } else {
                                if (type !== 'file') {
                                    $(that.options.target).find('[name="' + k + '"]').val(v);
                                }
                            }
                            break;
                    }
                }
            });
        }
    }
    this.setData = function(i) {
        var that = this;
        if (typeof i == 'object') {
            $.each(i, function(key, value) {
                $('#' + that.id).attr('data-' + key, value);
            });
        }
    }
    this.p = {
        app: false,
        autocomplete: false,
        bootstrap: true,
        control: false,
        data: false,
        event: false,
        form: false,
        name: false,
        returnHtml: false,
        setControlVars: true,
        setup: false,
        target: '#formModal .modal-body',
        type: 'data'
    }
    this.options = $.extend({}, this.p, inputObj);
    this.init();
};

function Field(field) {
    this.field = field;
    this.field.autocomplete = (typeof this.field.autocomplete ? this.field.autocomplete : '');
    this.fragment = document.createDocumentFragment();
    this.output = false;
    this.atts = {
        'class': 'form-control __' + String(this.field.name).replace('[]', '')
    }
    var d = new Date();
    this.id = 'id' + d.getTime() + Math.floor(Math.random() * 1000000);
    this.setAttributes = function(el) {
        var that = this;
        $.each(this.atts, function(key, value) {
            switch (key) {
                case 'autocomplete':
                    if (value == 'yes') {
                        el.setAttribute('list', that.id);
                        el.setAttribute('autocomplete', 'off');
                    }
                    break;
                    break;
                case 'options':
                case 'label':
                    break;
                case 'type':
                    if (value != 'textarea' && value != 'select' && value != 'multi-select') {
                        el.setAttribute(key, value);
                    }
                    break;
                default:
                    el.setAttribute(key, value);
                    break;
            }
        });
        return el;
    }
    var el, opt;
    var that = this;
    $.each(this.field, function(key, value) {
        if (value) {
            that.atts[key] = value;
        }
    });
    switch (this.field.type) {
        case 'multi-select':
        case 'select':
            el = document.createElement('select');
            if (this.atts.options) {
                var o = String(this.atts.options).split(',');
                $.each(o, function(key, value) {
                    opt = document.createElement('option');
                    opt.setAttribute('value', $.trim(value));
                    opt.appendChild(document.createTextNode($.trim(value)));
                    el.appendChild(opt);
                });
            }
            if (this.field.type == 'multi-select') {
                el.setAttribute('multiple', 'true');
            }
            el = this.setAttributes(el);
            this.fragment.appendChild(el);
            break;
        case 'multi-checkbox':
        case 'multi-radio':
            var type = (this.field.type == 'multi-radio' ? 'radio' : 'checkbox');
            var that = this;
            this.atts.label = false;
            if (this.atts.options) {
                var o = String(this.atts.options).split(',');
                var c, l, d, s;
                d = document.createElement('div');
                d.setAttribute('class', 'inputContainer');
                s = document.createElement('span');
                s.appendChild(document.createTextNode(String(that.atts.name).replace('[]', '')));
                d.appendChild(s);
                $.each(o, function(key, value) {
                    c = document.createElement('div');
                    c.setAttribute('class', type);
                    l = document.createElement('label');
                    el = document.createElement('input');
                    el.setAttribute('type', type);
                    el.setAttribute('value', value);
                    el.setAttribute('name', that.atts.name);
                    l.appendChild(el);
                    l.appendChild(document.createTextNode(value));
                    c.appendChild(l);
                    d.appendChild(c);
                });
                that.fragment.appendChild(d);
            }
            break;
        case 'multi-text':
            this.atts.label = false;
            var a = (this.atts.type ? 'type="' + this.atts.type + '" ' : '') +
                (this.atts.name ? 'name="' + this.atts.name + '" ' : '') +
                (this.atts.placeholder ? 'placeholder="' + this.atts.placeholder + '" ' : '') +
                (this.atts.required ? 'required="required" ' : '');
            this.output = '<label style="text-transform:capitalize;" class="__' + this.atts.type + '">' + String(this.atts.name).replace('[]', '') + '<div class="input-group">' + '<input class="form-control" type="text"' + a + '/>' + '<span class="input-group-btn"><button onclick="$(\'.quantumLabelF:last\').remove();return false;" class="btn btn-default"><i class="fa fa-minus"></i></button>' + '<button onclick="$(this).parents(\'label:first\').after(\'<label class=&quot;quantumLabelF&quot;>\'+$(this).parents(\'.input-group:first\').find(\'input:first\').outerHTML()+\'</label>\');' + '$(\'#quantumLabelLast:first\');return false;" class="quantumLabelFAdder btn btn-default" type="button"><i class="fa fa-plus"></i></button></span>' + '</div></label>';
            break;
        case 'textarea':
            el = document.createElement('textarea');
            el = this.setAttributes(el);
            el.setAttribute('name', that.field.name);
            this.fragment.appendChild(el);
            break;
        default:
            el = document.createElement('input');
            el = this.setAttributes(el);
            this.fragment.appendChild(el);
            break;
    }
    if (this.output === false) {
        var div = document.createElement('div');
        div.appendChild(this.fragment.cloneNode(true));
        if (this.atts.label) {
            this.output = '<label class="__' + this.atts.type + '"><span>' + this.atts.label + '</span><br />' + div.innerHTML + '</label>';
        } else {
            this.output = div.innerHTML;
        }
        if (this.atts.autocomplete == 'yes') {
            this.output += '<datalist id="' + this.id + '"></datalist>';
        }
    }
};

function Table(i) {
    this.data = false;
    this.meta = false;
    this.fields = false;
    this.selector = false;
    this.activePage = 1;
    if (typeof i.data != 'undefined') {
        if (typeof i.fields != 'undefined') {
            var p = {}
            $.each(i.fields, function(key, value) {
                p[value] = '';
            });
            $.each(i.data, function(key, value) {
                i.data[key] = $.extend({}, p, value);
            });
        }
        this.data = i.data;
    }
    if (typeof i.meta != 'undefined') {
        this.meta = i.meta;
    }
    if (typeof i.fields != 'undefined') {
        this.fields = i.fields;
    }
    if (typeof i.selector != 'undefined') {
        this.selector = i.selector;
    }
    this.create = function() {
        if (this.fields && this.data && this.meta && this.selector) {
            var fragment = document.createDocumentFragment(),
                tr, td, i, il, a, key, checkbox;
            var c = 0;
            for (var id in this.data) {
                if (this.data.hasOwnProperty(id)) {
                    var end = this.meta.start + this.meta.max;
                    if (c >= this.meta.start && c <= end) {
                        tr = document.createElement('tr');
                        tr.setAttribute('data-id', id);
                        checkbox = document.createElement('input');
                        checkbox.setAttribute('type', 'checkbox');
                        checkbox.removeAttribute('id');
                        td = document.createElement('td');
                        td.appendChild(checkbox);
                        tr.appendChild(td);
                        for (key in this.data[id]) {
                            td = document.createElement('td');
                            td.setAttribute('data-th', String(key).split('_').join(' ') + ': ');
                            a = document.createElement('a');
                            a.setAttribute('href', '#');
                            a.setAttribute('title', this.data[id][key]);
                            a.appendChild(document.createTextNode(this.data[id][key]));
                            td.appendChild(a);
                            tr.appendChild(td);
                        }
                        fragment.appendChild(tr);
                    }
                }
                c++;
            }
            $(this.selector).html('');
            var shell = '<div class="fileLabelSection"><div class="table-responsive fileLabelContainer"><table class="fileLabelTable table table-bordered table-striped"></table></div>';
            $(this.selector).html(shell);
            $(this.selector).find('table').append(this.tableHead()).append(fragment);
            $(this.selector).trigger('renderTable');
        }
    }
    this.page = function() {
        this.meta.start = this.activePage * this.meta.max;
        if (this.activePage == 1) {
            this.meta.start = 0;
        }
        this.create();
    }
    this.pagination = function() {
        var pages = Math.ceil(this.meta.count / this.meta.max) - 1;
        if (!pages) {
            pages = 1;
        }
        var html = '<div class="fileLabelPagination text-center">' + '<nav>' + '  <ul class="pagination pagination-sm">' + '    <li><a title="First Page" href="#"><span aria-hidden="true">&laquo;</span><span class="sr-only">Previous</span></a></li>';
        for (var n = 1; n <= pages; n++) {
            html += '<li data-page="' + n + '" class="hidden page"><a href="#">' + n + '</a></li>';
        }
        html += '<li><a title="Last Page" href="#"><span aria-hidden="true">&raquo;</span><span class="sr-only">Next</span></a></li></ul></nav></div>';
        $('.fileLabelPagination').remove();
        $(this.selector).before(html);
        $(this.selector).after(html);
        var that = this;
        $(document).off('click', '[title="First Page"]');
        $(document).on('click', '[title="First Page"]', function() {
            $('.fileLabelPagination .page').addClass('hidden');
            for (var n = 1; n <= 5; n++) {
                $('[data-page="' + n + '"]').removeClass('hidden');
            }
            $('[data-page="1"]').trigger('click');
        });
        $(document).off('click', '[title="Last Page"]');
        $(document).on('click', '[title="Last Page"]', function(e) {
            e.preventDefault();
            $('.fileLabelPagination .page').addClass('hidden');
            var p = pages - 5;
            for (var n = p; n <= pages; n++) {
                $('[data-page="' + n + '"]').removeClass('hidden');
            }
            $('[data-page="' + pages + '"]').trigger('click');
        });
        $(document).off('click', '.fileLabelPagination .page');
        $(document).on('click', '.fileLabelPagination .page', function(e) {
            e.preventDefault();
            that.activePage = parseInt($(this).attr('data-page'));
            if ($(this).next().hasClass('hidden')) {
                var next = parseInt($(this).next().attr('data-page'));
                var first = next - 5;
                $('[data-page="' + next + '"]').removeClass('hidden');
                $('[data-page="' + first + '"]').addClass('hidden');
            }
            $('.page').removeClass('active');
            $('[data-page="' + that.activePage + '"]').addClass('active');
            that.page();
        });
        if (this.activePage == 1) {
            for (var n = 1; n <= 5; n++) {
                $('[data-page="' + n + '"]').removeClass('hidden');
            }
            $('[data-page="1"]').addClass('active');
        }
        $('body').trigger('renderPagination');
    }
    this.tableHead = function() {
        var fragment = document.createDocumentFragment(),
            tr, th, i, il, key, thead, div, cb;
        thead = document.createElement('thead');
        tr = document.createElement('tr');
        th = document.createElement('th');
        cb = document.createElement('input');
        cb.setAttribute('type', 'checkbox');
        cb.setAttribute('id', 'checkAll');
        th.appendChild(cb);
        tr.appendChild(th);
        for (field in this.fields) {
            th = document.createElement('th');
            th.appendChild(document.createTextNode(String(this.fields[field]).split('_').join(' ')));
            tr.appendChild(th);
        }
        thead.appendChild(tr);
        fragment.appendChild(thead);
        return fragment;
    }
    this.create();
    if (this.meta.count > this.meta.max) {
        this.pagination();
    } else {
        $('.fileLabelPagination').remove();
    }
};

function Template(i, callback) {
    var d = {
        'type': 'dl',
        'data': {},
        'exclude': [],
        'fields': {},
        'checkboxes': false,
        'ddTag': 'a',
        'editable': false,
        'classes': ''
    }
    this.options = $.extend({}, d, i);
    var f = this.options.type;
    if (JSON.stringify(this.options.fields) == '{}') {
        var keys = Object.keys(this.options.data);
        if (typeof this.options.data[keys[0]] == 'object') {
            this.options.fields = Object.keys(this.options.data[keys[0]]);
        }
    }
    this.dl = function() {
        var that = this;
        var html = '<dl data-id="' + (typeof that.options.data.id == 'undefined' ? 'none' : that.options.data.id) + '" data-uri="' + encodeURI($.param(that.options.data)) + '" class="dl-horizontal' +
            (that.options.classes ? ' ' + that.options.classes : '') + '">';
        $.each(that.options.data, function(key, value) {
            if ($.inArray(key, that.options.exclude) == -1) {
                var k = library.ucwords(String(key).replace('_', ' '));
                if (that.options.ddTag) {
                    switch (that.options.ddTag) {
                        case 'a':
                            html += '<dt>' + k + '</dt><dd><a data-key="' + key + '" contenteditable="' + String(that.options.editable) + '" data-value="' + value + '" href="#">' + value + '</a></dd>';
                            break;
                        default:
                            html += '<dt>' + k + '</dt><dd><' + that.options.ddTag + ' contenteditable="' + String(that.options.editable) + '" data-key="' + key + '" data-value="' + value + '">' + value + '</' + that.options.ddTag + '></dd>';
                            break;
                    }
                } else {
                    html += '<dt>' + k + '</dt><dd contenteditable="' + String(that.options.editable) + '">' + value + '</dd>';
                }
            }
        });
        html += '</dl>';
        return html;
    }
    this.table = function() {
        var that = this;
        var html = '<div class="table-responsive"><table class="table table-hover table-bordered table-striped' + (that.options.classes ? ' ' + that.options.classes : '') + '"><tr>';
        if (this.options.checkboxes) {
            html += '<th><input type="checkbox" id="checkAll"></th>';
        }
        $.each(that.options.fields, function(key, value) {
            html += '<th>' + value + '</th>';
        });
        html += '</tr>';
        $.each(that.options.data, function(id, obj) {
            html += '<tr data-id="' + id + '">';
            if (that.options.checkboxes) {
                html += '<td><input type="checkbox"></td>';
            }
            $.each(obj, function(key, value) {
                if ($.inArray(key, that.options.fields) !== -1) {
                    html += '<td contenteditable="' + String(that.options.editable) + '" class="' + key + '">' + value + '</td>';
                }
            });
            html += '</tr>';
        });
        html += '</table></div>';
        return html;
    }
    this.output = '';
    switch (this.options.type) {
        case 'dl':
            this.output = this.dl();
            break;
        case 'table':
            this.output = this.table();
            break;
    }
    if (typeof callback != 'undefined') {
        callback(this);
    }
};

function simpleDB(type) {
    this.type = type;
    if (!localStorage.getItem('simpleDB')) localStorage.setItem('simpleDB', JSON.stringify({}));
    this.db = JSON.parse(localStorage.getItem('simpleDB'));
    if (!this.db.hasOwnProperty(this.type)) {
        this.db[this.type] = {}
        localStorage.setItem('simpleDB', JSON.stringify(this.db));
    }
    this.db = JSON.parse(localStorage.getItem('simpleDB'));
    this.arrayShift = function(inputArr) {
        var props = false,
            shift = undefined,
            pr = '',
            allDigits = /^\d$/,
            int_ct = -1,
            _checkToUpIndices = function(arr, ct, key) {
                if (arr[ct] !== undefined) {
                    var tmp = ct;
                    ct += 1;
                    if (ct === key) {
                        ct += 1;
                    }
                    ct = _checkToUpIndices(arr, ct, key);
                    arr[ct] = arr[tmp];
                    delete arr[tmp];
                }
                return ct;
            };
        if (inputArr.length === 0) {
            return null;
        }
        if (inputArr.length > 0) {
            return inputArr.shift();
        }
    }
    this.check = function(key) {
        if (!this.db[this.type].hasOwnProperty(key)) {
            return key;
        } else {
            return this.check(this.uniqid());
        }
    }
    this.count = function(mixed_var, mode) {
        var key, cnt = 0;
        if (mixed_var === null || typeof mixed_var === 'undefined') {
            return 0;
        } else if (mixed_var.constructor !== Array && mixed_var.constructor !== Object) {
            return 1;
        }
        if (mode === 'COUNT_RECURSIVE') {
            mode = 1;
        }
        if (mode != 1) {
            mode = 0;
        }
        for (key in mixed_var) {
            if (mixed_var.hasOwnProperty(key)) {
                cnt++;
                if (mode == 1 && mixed_var[key] && (mixed_var[key].constructor === Array || mixed_var[key].constructor === Object)) {
                    cnt += this.count(mixed_var[key], 1);
                }
            }
        }
        return cnt;
    }
    this.del = function(key) {
        this.getDB();
        try {
            var type = this.db[this.type];
        } catch (err) {
            var type = this.db[this.type];
        }
        delete type[key];
        this.db[this.type] = type;
        localStorage.setItem('simpleDB', JSON.stringify(this.db));
    }
    this.get = function(key) {
        this.getDB();
        if (typeof key == 'undefined') {
            var output = []
            for (var key in this.db[this.type]) {
                try {
                    output[key] = JSON.parse(this.db[this.type][key]);
                } catch (err) {
                    output[key] = this.db[this.type][key];
                }
            }
            return this.toObject(output);
        } else {
            try {
                return this.db[this.type][key];
            } catch (err) {
                return false;
            }
        }
        return false;
    }
    this.getDB = function() {
        this.db = JSON.parse(localStorage.getItem('simpleDB'));
    }
    this.getJSON = function(key) {
        this.getDB();
        return JSON.stringfy(this.db[this.type][key]);
    }
    this.objectToQueryString = function(obj) {
        var output = '';
        var count = 0;
        for (var key in obj) {
            if (obj.hasOwnProperty(key)) {
                output += key + '=' + obj[key] + '&';
            }
        }
        return output.slice(0, -1);
    }
    this.post = function(content) {
        this.getDB();
        var key = this.check(this.uniqid());
        this.db[this.type][key] = content;
        localStorage.setItem('simpleDB', JSON.stringify(this.db));
        return key;
    }
    this.parseStr = function(q) {
        var arr = {};
        var query = String(q).split('&');
        for (var key in query) {
            if (query.hasOwnProperty(key)) {
                var str = String(query[key]).split('=');
                var va = str[1];
                arr[str[0]] = str[1];
            }
        }
        return arr;
    }
    this.put = function(id, content) {
        this.getDB();
        var key = id;
        this.db[this.type][key] = content;
        localStorage.setItem('simpleDB', JSON.stringify(this.db));
        return key;
    }
    this.query = function(q) {
        var arr = this.parseStr(q);
        var get = this.get();
        var out = {};
        var empty = true;
        if (get) {
            for (var key in get) {
                if (get.hasOwnProperty(key)) {
                    var objectString = this.objectToQueryString(get[key]);
                    if (this.testQueryArray(arr, objectString)) {
                        var empty = false;
                        out[key] = get[key];
                    }
                }
            }
        }
        if (empty) {
            return false;
        } else {
            return out;
        }
    }
    this.returnSingleId = function(a) {
        if (a) {
            for (var key in a) {
                if (a.hasOwnProperty(key)) {
                    return key;
                }
            }
        } else {
            return false;
        }
    }, this.shiftArray = function(arr) {
        if (this.count(arr) == 1) {
            return this.shiftArray(arr);
        } else {
            return arr;
        }
    }
    this.testQueryArray = function(array, objectString) {
        var testArray = [];
        for (var key in array) {
            if (array.hasOwnProperty(key)) {
                testArray.push(key + '=' + array[key]);
            }
        }
        var count = this.count(testArray);
        var testCount = 0;
        for (var key in testArray) {
            if (testArray.hasOwnProperty(key)) {
                if (objectString.indexOf(testArray[key]) !== -1) {
                    testCount++;
                }
            }
        }
        if (count == testCount) {
            return true;
        } else {
            return false;
        }
    }
    this.timestamp = function(id) {
        var str = String(id).replace('id');
        return parseInt(str);
    }
    this.toObject = function(arr) {
        var rv = {};
        for (var key in arr) {
            if (arr.hasOwnProperty(key)) {
                rv[key] = arr[key];
            }
        }
        return rv;
    }
    this.uniqid = function() {
        var d = new Date();
        return 'id' + d.getTime() + Math.floor(Math.random() * 1000000);
    }
}

function setlocale(category, locale) {
  //  discuss at: http://phpjs.org/functions/setlocale/
  // original by: Brett Zamir (http://brett-zamir.me)
  // original by: Blues at http://hacks.bluesmoon.info/strftime/strftime.js
  // original by: YUI Library: http://developer.yahoo.com/yui/docs/YAHOO.util.DateLocale.html
  //  depends on: getenv
  //        note: Is extensible, but currently only implements locales en,
  //        note: en_US, en_GB, en_AU, fr, and fr_CA for LC_TIME only; C for LC_CTYPE;
  //        note: C and en for LC_MONETARY/LC_NUMERIC; en for LC_COLLATE
  //        note: Uses global: php_js to store locale info
  //        note: Consider using http://demo.icu-project.org/icu-bin/locexp as basis for localization (as in i18n_loc_set_default())
  //   example 1: setlocale('LC_ALL', 'en_US');
  //   returns 1: 'en_US'

  var categ = '',
    cats = [],
    i = 0,
    d = this.window.document;

  // BEGIN STATIC
  var _copy = function _copy(orig) {
    if (orig instanceof RegExp) {
      return new RegExp(orig);
    } else if (orig instanceof Date) {
      return new Date(orig);
    }
    var newObj = {};
    for (var i in orig) {
      if (typeof orig[i] === 'object') {
        newObj[i] = _copy(orig[i]);
      } else {
        newObj[i] = orig[i];
      }
    }
    return newObj;
  };

  // Function usable by a ngettext implementation (apparently not an accessible part of setlocale(), but locale-specific)
  // See http://www.gnu.org/software/gettext/manual/gettext.html#Plural-forms though amended with others from
  // https://developer.mozilla.org/En/Localization_and_Plurals (new categories noted with "MDC" below, though
  // not sure of whether there is a convention for the relative order of these newer groups as far as ngettext)
  // The function name indicates the number of plural forms (nplural)
  // Need to look into http://cldr.unicode.org/ (maybe future JavaScript); Dojo has some functions (under new BSD),
  // including JSON conversions of LDML XML from CLDR: http://bugs.dojotoolkit.org/browser/dojo/trunk/cldr
  // and docs at http://api.dojotoolkit.org/jsdoc/HEAD/dojo.cldr
  var _nplurals1 = function(n) {
    // e.g., Japanese
    return 0;
  };
  var _nplurals2a = function(n) {
    // e.g., English
    return n !== 1 ? 1 : 0;
  };
  var _nplurals2b = function(n) {
    // e.g., French
    return n > 1 ? 1 : 0;
  };
  var _nplurals2c = function(n) {
    // e.g., Icelandic (MDC)
    return n % 10 === 1 && n % 100 !== 11 ? 0 : 1;
  };
  var _nplurals3a = function(n) {
    // e.g., Latvian (MDC has a different order from gettext)
    return n % 10 === 1 && n % 100 !== 11 ? 0 : n !== 0 ? 1 : 2;
  };
  var _nplurals3b = function(n) {
    // e.g., Scottish Gaelic
    return n === 1 ? 0 : n === 2 ? 1 : 2;
  };
  var _nplurals3c = function(n) {
    // e.g., Romanian
    return n === 1 ? 0 : (n === 0 || (n % 100 > 0 && n % 100 < 20)) ? 1 : 2;
  };
  var _nplurals3d = function(n) {
    // e.g., Lithuanian (MDC has a different order from gettext)
    return n % 10 === 1 && n % 100 !== 11 ? 0 : n % 10 >= 2 && (n % 100 < 10 || n % 100 >= 20) ? 1 : 2;
  };
  var _nplurals3e = function(n) {
    // e.g., Croatian
    return n % 10 === 1 && n % 100 !== 11 ? 0 : n % 10 >= 2 && n % 10 <= 4 && (n % 100 < 10 || n % 100 >= 20) ? 1 :
      2;
  };
  var _nplurals3f = function(n) {
    // e.g., Slovak
    return n === 1 ? 0 : n >= 2 && n <= 4 ? 1 : 2;
  };
  var _nplurals3g = function(n) {
    // e.g., Polish
    return n === 1 ? 0 : n % 10 >= 2 && n % 10 <= 4 && (n % 100 < 10 || n % 100 >= 20) ? 1 : 2;
  };
  var _nplurals3h = function(n) {
    // e.g., Macedonian (MDC)
    return n % 10 === 1 ? 0 : n % 10 === 2 ? 1 : 2;
  };
  var _nplurals4a = function(n) {
    // e.g., Slovenian
    return n % 100 === 1 ? 0 : n % 100 === 2 ? 1 : n % 100 === 3 || n % 100 === 4 ? 2 : 3;
  };
  var _nplurals4b = function(n) {
    // e.g., Maltese (MDC)
    return n === 1 ? 0 : n === 0 || (n % 100 && n % 100 <= 10) ? 1 : n % 100 >= 11 && n % 100 <= 19 ? 2 : 3;
  };
  var _nplurals5 = function(n) {
    // e.g., Irish Gaeilge (MDC)
    return n === 1 ? 0 : n === 2 ? 1 : n >= 3 && n <= 6 ? 2 : n >= 7 && n <= 10 ? 3 : 4;
  };
  var _nplurals6 = function(n) {
    // e.g., Arabic (MDC) - Per MDC puts 0 as last group
    return n === 0 ? 5 : n === 1 ? 0 : n === 2 ? 1 : n % 100 >= 3 && n % 100 <= 10 ? 2 : n % 100 >= 11 && n % 100 <=
      99 ? 3 : 4;
  };
  // END STATIC
  // BEGIN REDUNDANT
  try {
    this.php_js = this.php_js || {};
  } catch (e) {
    this.php_js = {};
  }

  var phpjs = this.php_js;

  // Reconcile Windows vs. *nix locale names?
  // Allow different priority orders of languages, esp. if implement gettext as in
  //     LANGUAGE env. var.? (e.g., show German if French is not available)
  if (!phpjs.locales) {
    // Can add to the locales
    phpjs.locales = {};

    phpjs.locales.en = {
      'LC_COLLATE'  : // For strcoll

        function(str1, str2) {
        // Fix: This one taken from strcmp, but need for other locales; we don't use localeCompare since its locale is not settable
        return (str1 == str2) ? 0 : ((str1 > str2) ? 1 : -1);
      },
      'LC_CTYPE'    : {
        // Need to change any of these for English as opposed to C?
        an      : /^[A-Za-z\d]+$/g,
        al      : /^[A-Za-z]+$/g,
        ct      : /^[\u0000-\u001F\u007F]+$/g,
        dg      : /^[\d]+$/g,
        gr      : /^[\u0021-\u007E]+$/g,
        lw      : /^[a-z]+$/g,
        pr      : /^[\u0020-\u007E]+$/g,
        pu      : /^[\u0021-\u002F\u003A-\u0040\u005B-\u0060\u007B-\u007E]+$/g,
        sp      : /^[\f\n\r\t\v ]+$/g,
        up      : /^[A-Z]+$/g,
        xd      : /^[A-Fa-f\d]+$/g,
        CODESET : 'UTF-8',
        // Used by sql_regcase
        lower   : 'abcdefghijklmnopqrstuvwxyz',
        upper   : 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
      },
      'LC_TIME'     : {
        // Comments include nl_langinfo() constant equivalents and any changes from Blues' implementation
        a           : ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
        // ABDAY_
        A           : ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
        // DAY_
        b           : ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
        // ABMON_
        B           : ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October',
          'November', 'December'
        ],
        // MON_
        c           : '%a %d %b %Y %r %Z',
        // D_T_FMT // changed %T to %r per results
        p           : ['AM', 'PM'],
        // AM_STR/PM_STR
        P           : ['am', 'pm'],
        // Not available in nl_langinfo()
        r           : '%I:%M:%S %p',
        // T_FMT_AMPM (Fixed for all locales)
        x           : '%m/%d/%Y',
        // D_FMT // switched order of %m and %d; changed %y to %Y (C uses %y)
        X           : '%r',
        // T_FMT // changed from %T to %r  (%T is default for C, not English US)
        // Following are from nl_langinfo() or http://www.cptec.inpe.br/sx4/sx4man2/g1ab02e/strftime.4.html
        alt_digits  : '',
        // e.g., ordinal
        ERA         : '',
        ERA_YEAR    : '',
        ERA_D_T_FMT : '',
        ERA_D_FMT   : '',
        ERA_T_FMT   : ''
      },
      // Assuming distinction between numeric and monetary is thus:
      // See below for C locale
      'LC_MONETARY' : {
        // based on Windows "english" (English_United States.1252) locale
        int_curr_symbol   : 'USD',
        currency_symbol   : '$',
        mon_decimal_point : '.',
        mon_thousands_sep : ',',
        mon_grouping      : [3],
        // use mon_thousands_sep; "" for no grouping; additional array members indicate successive group lengths after first group (e.g., if to be 1,23,456, could be [3, 2])
        positive_sign     : '',
        negative_sign     : '-',
        int_frac_digits   : 2,
        // Fractional digits only for money defaults?
        frac_digits       : 2,
        p_cs_precedes     : 1,
        // positive currency symbol follows value = 0; precedes value = 1
        p_sep_by_space    : 0,
        // 0: no space between curr. symbol and value; 1: space sep. them unless symb. and sign are adjacent then space sep. them from value; 2: space sep. sign and value unless symb. and sign are adjacent then space separates
        n_cs_precedes     : 1,
        // see p_cs_precedes
        n_sep_by_space    : 0,
        // see p_sep_by_space
        p_sign_posn       : 3,
        // 0: parentheses surround quantity and curr. symbol; 1: sign precedes them; 2: sign follows them; 3: sign immed. precedes curr. symbol; 4: sign immed. succeeds curr. symbol
        n_sign_posn       : 0 // see p_sign_posn
      },
      'LC_NUMERIC'  : {
        // based on Windows "english" (English_United States.1252) locale
        decimal_point : '.',
        thousands_sep : ',',
        grouping      : [3] // see mon_grouping, but for non-monetary values (use thousands_sep)
      },
      'LC_MESSAGES' : {
        YESEXPR : '^[yY].*',
        NOEXPR  : '^[nN].*',
        YESSTR  : '',
        NOSTR   : ''
      },
      nplurals      : _nplurals2a
    };
    phpjs.locales.en_US = _copy(phpjs.locales.en);
    phpjs.locales.en_US.LC_TIME.c = '%a %d %b %Y %r %Z';
    phpjs.locales.en_US.LC_TIME.x = '%D';
    phpjs.locales.en_US.LC_TIME.X = '%r';
    // The following are based on *nix settings
    phpjs.locales.en_US.LC_MONETARY.int_curr_symbol = 'USD ';
    phpjs.locales.en_US.LC_MONETARY.p_sign_posn = 1;
    phpjs.locales.en_US.LC_MONETARY.n_sign_posn = 1;
    phpjs.locales.en_US.LC_MONETARY.mon_grouping = [3, 3];
    phpjs.locales.en_US.LC_NUMERIC.thousands_sep = '';
    phpjs.locales.en_US.LC_NUMERIC.grouping = [];

    phpjs.locales.en_GB = _copy(phpjs.locales.en);
    phpjs.locales.en_GB.LC_TIME.r = '%l:%M:%S %P %Z';

    phpjs.locales.en_AU = _copy(phpjs.locales.en_GB);
    // Assume C locale is like English (?) (We need C locale for LC_CTYPE)
    phpjs.locales.C = _copy(phpjs.locales.en);
    phpjs.locales.C.LC_CTYPE.CODESET = 'ANSI_X3.4-1968';
    phpjs.locales.C.LC_MONETARY = {
      int_curr_symbol   : '',
      currency_symbol   : '',
      mon_decimal_point : '',
      mon_thousands_sep : '',
      mon_grouping      : [],
      p_cs_precedes     : 127,
      p_sep_by_space    : 127,
      n_cs_precedes     : 127,
      n_sep_by_space    : 127,
      p_sign_posn       : 127,
      n_sign_posn       : 127,
      positive_sign     : '',
      negative_sign     : '',
      int_frac_digits   : 127,
      frac_digits       : 127
    };
    phpjs.locales.C.LC_NUMERIC = {
      decimal_point : '.',
      thousands_sep : '',
      grouping      : []
    };
    // D_T_FMT
    phpjs.locales.C.LC_TIME.c = '%a %b %e %H:%M:%S %Y';
    // D_FMT
    phpjs.locales.C.LC_TIME.x = '%m/%d/%y';
    // T_FMT
    phpjs.locales.C.LC_TIME.X = '%H:%M:%S';
    phpjs.locales.C.LC_MESSAGES.YESEXPR = '^[yY]';
    phpjs.locales.C.LC_MESSAGES.NOEXPR = '^[nN]';

    phpjs.locales.fr = _copy(phpjs.locales.en);
    phpjs.locales.fr.nplurals = _nplurals2b;
    phpjs.locales.fr.LC_TIME.a = ['dim', 'lun', 'mar', 'mer', 'jeu', 'ven', 'sam'];
    phpjs.locales.fr.LC_TIME.A = ['dimanche', 'lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi'];
    phpjs.locales.fr.LC_TIME.b = ['jan', 'f\u00E9v', 'mar', 'avr', 'mai', 'jun', 'jui', 'ao\u00FB', 'sep', 'oct',
      'nov', 'd\u00E9c'
    ];
    phpjs.locales.fr.LC_TIME.B = ['janvier', 'f\u00E9vrier', 'mars', 'avril', 'mai', 'juin', 'juillet', 'ao\u00FBt',
      'septembre', 'octobre', 'novembre', 'd\u00E9cembre'
    ];
    phpjs.locales.fr.LC_TIME.c = '%a %d %b %Y %T %Z';
    phpjs.locales.fr.LC_TIME.p = ['', ''];
    phpjs.locales.fr.LC_TIME.P = ['', ''];
    phpjs.locales.fr.LC_TIME.x = '%d.%m.%Y';
    phpjs.locales.fr.LC_TIME.X = '%T';

    phpjs.locales.fr_CA = _copy(phpjs.locales.fr);
    phpjs.locales.fr_CA.LC_TIME.x = '%Y-%m-%d';
  }
  if (!phpjs.locale) {
    phpjs.locale = 'en_US';
    var NS_XHTML = 'http://www.w3.org/1999/xhtml';
    var NS_XML = 'http://www.w3.org/XML/1998/namespace';
    if (d.getElementsByTagNameNS && d.getElementsByTagNameNS(NS_XHTML, 'html')[0]) {
      if (d.getElementsByTagNameNS(NS_XHTML, 'html')[0].getAttributeNS && d.getElementsByTagNameNS(NS_XHTML,
          'html')[0].getAttributeNS(NS_XML, 'lang')) {
        phpjs.locale = d.getElementsByTagName(NS_XHTML, 'html')[0].getAttributeNS(NS_XML, 'lang');
      } else if (d.getElementsByTagNameNS(NS_XHTML, 'html')[0].lang) {
        // XHTML 1.0 only
        phpjs.locale = d.getElementsByTagNameNS(NS_XHTML, 'html')[0].lang;
      }
    } else if (d.getElementsByTagName('html')[0] && d.getElementsByTagName('html')[0].lang) {
      phpjs.locale = d.getElementsByTagName('html')[0].lang;
    }
  }
  // PHP-style
  phpjs.locale = phpjs.locale.replace('-', '_');
  // Fix locale if declared locale hasn't been defined
  if (!(phpjs.locale in phpjs.locales)) {
    if (phpjs.locale.replace(/_[a-zA-Z]+$/, '') in phpjs.locales) {
      phpjs.locale = phpjs.locale.replace(/_[a-zA-Z]+$/, '');
    }
  }

  if (!phpjs.localeCategories) {
    phpjs.localeCategories = {
      'LC_COLLATE'  : phpjs.locale,
      // for string comparison, see strcoll()
      'LC_CTYPE'    : phpjs.locale,
      // for character classification and conversion, for example strtoupper()
      'LC_MONETARY' : phpjs.locale,
      // for localeconv()
      'LC_NUMERIC'  : phpjs.locale,
      // for decimal separator (See also localeconv())
      'LC_TIME'     : phpjs.locale,
      // for date and time formatting with strftime()
      'LC_MESSAGES' : phpjs.locale // for system responses (available if PHP was compiled with libintl)
    };
  }
  // END REDUNDANT
  if (locale === null || locale === '') {
    locale = this.getenv(category) || this.getenv('LANG');
  } else if (Object.prototype.toString.call(locale) === '[object Array]') {
    for (i = 0; i < locale.length; i++) {
      if (!(locale[i] in this.php_js.locales)) {
        if (i === locale.length - 1) {
          // none found
          return false;
        }
        continue;
      }
      locale = locale[i];
      break;
    }
  }

  // Just get the locale
  if (locale === '0' || locale === 0) {
    if (category === 'LC_ALL') {
      for (categ in this.php_js.localeCategories) {
        // Add ".UTF-8" or allow ".@latint", etc. to the end?
        cats.push(categ + '=' + this.php_js.localeCategories[categ]);
      }
      return cats.join(';');
    }
    return this.php_js.localeCategories[category];
  }

  if (!(locale in this.php_js.locales)) {
    // Locale not found
    return false;
  }

  // Set and get locale
  if (category === 'LC_ALL') {
    for (categ in this.php_js.localeCategories) {
      this.php_js.localeCategories[categ] = locale;
    }
  } else {
    this.php_js.localeCategories[category] = locale;
  }
  return locale;
}

function money_format(format, number) {
  //  discuss at: http://phpjs.org/functions/money_format/
  // original by: Brett Zamir (http://brett-zamir.me)
  //    input by: daniel airton wermann (http://wermann.com.br)
  // bugfixed by: Brett Zamir (http://brett-zamir.me)
  //  depends on: setlocale
  //        note: This depends on setlocale having the appropriate
  //        note: locale (these examples use 'en_US')
  //   example 1: money_format('%i', 1234.56);
  //   returns 1: ' USD 1,234.56'
  //   example 2: money_format('%14#8.2n', 1234.5678);
  //   returns 2: ' $     1,234.57'
  //   example 3: money_format('%14#8.2n', -1234.5678);
  //   returns 3: '-$     1,234.57'
  //   example 4: money_format('%(14#8.2n', 1234.5678);
  //   returns 4: ' $     1,234.57 '
  //   example 5: money_format('%(14#8.2n', -1234.5678);
  //   returns 5: '($     1,234.57)'
  //   example 6: money_format('%=014#8.2n', 1234.5678);
  //   returns 6: ' $000001,234.57'
  //   example 7: money_format('%=014#8.2n', -1234.5678);
  //   returns 7: '-$000001,234.57'
  //   example 8: money_format('%=*14#8.2n', 1234.5678);
  //   returns 8: ' $*****1,234.57'
  //   example 9: money_format('%=*14#8.2n', -1234.5678);
  //   returns 9: '-$*****1,234.57'
  //  example 10: money_format('%=*^14#8.2n', 1234.5678);
  //  returns 10: '  $****1234.57'
  //  example 11: money_format('%=*^14#8.2n', -1234.5678);
  //  returns 11: ' -$****1234.57'
  //  example 12: money_format('%=*!14#8.2n', 1234.5678);
  //  returns 12: ' *****1,234.57'
  //  example 13: money_format('%=*!14#8.2n', -1234.5678);
  //  returns 13: '-*****1,234.57'
  //  example 14: money_format('%i', 3590);
  //  returns 14: ' USD 3,590.00'

  // Per PHP behavior, there seems to be no extra padding for sign when there is a positive number, though my
  // understanding of the description is that there should be padding; need to revisit examples

  // Helpful info at http://ftp.gnu.org/pub/pub/old-gnu/Manuals/glibc-2.2.3/html_chapter/libc_7.html and http://publib.boulder.ibm.com/infocenter/zos/v1r10/index.jsp?topic=/com.ibm.zos.r10.bpxbd00/strfmp.htm

  if (typeof number !== 'number') {
    return null;
  }
  // 1: flags, 3: width, 5: left, 7: right, 8: conversion
  var regex = /%((=.|[+^(!-])*?)(\d*?)(#(\d+))?(\.(\d+))?([in%])/g;

  // Ensure the locale data we need is set up
  this.setlocale('LC_ALL', 0);
  var monetary = this.php_js.locales[this.php_js.localeCategories['LC_MONETARY']]['LC_MONETARY'];

  var doReplace = function(n0, flags, n2, width, n4, left, n6, right, conversion) {
    var value = '',
      repl = '';
    if (conversion === '%') {
      // Percent does not seem to be allowed with intervening content
      return '%';
    }
    var fill = flags && (/=./)
      .test(flags) ? flags.match(/=(.)/)[1] : ' '; // flag: =f (numeric fill)
    // flag: ! (suppress currency symbol)
    var showCurrSymbol = !flags || flags.indexOf('!') === -1;
    // field width: w (minimum field width)
    width = parseInt(width, 10) || 0;

    var neg = number < 0;
    // Convert to string
    number = number + '';
    // We don't want negative symbol represented here yet
    number = neg ? number.slice(1) : number;

    var decpos = number.indexOf('.');
    // Get integer portion
    var integer = decpos !== -1 ? number.slice(0, decpos) : number;
    // Get decimal portion
    var fraction = decpos !== -1 ? number.slice(decpos + 1) : '';

    var _str_splice = function(integerStr, idx, thous_sep) {
      var integerArr = integerStr.split('');
      integerArr.splice(idx, 0, thous_sep);
      return integerArr.join('');
    };

    var init_lgth = integer.length;
    left = parseInt(left, 10);
    var filler = init_lgth < left;
    if (filler) {
      var fillnum = left - init_lgth;
      integer = new Array(fillnum + 1)
        .join(fill) + integer;
    }
    if (flags.indexOf('^') === -1) {
      // flag: ^ (disable grouping characters (of locale))
      // use grouping characters
      // ','
      var thous_sep = monetary.mon_thousands_sep;
      // [3] (every 3 digits in U.S.A. locale)
      var mon_grouping = monetary.mon_grouping;

      if (mon_grouping[0] < integer.length) {
        for (var i = 0, idx = integer.length; i < mon_grouping.length; i++) {
          // e.g., 3
          idx -= mon_grouping[i];
          if (idx <= 0) {
            break;
          }
          if (filler && idx < fillnum) {
            thous_sep = fill;
          }
          integer = _str_splice(integer, idx, thous_sep);
        }
      }
      if (mon_grouping[i - 1] > 0) {
        // Repeating last grouping (may only be one) until highest portion of integer reached
        while (idx > mon_grouping[i - 1]) {
          idx -= mon_grouping[i - 1];
          if (filler && idx < fillnum) {
            thous_sep = fill;
          }
          integer = _str_splice(integer, idx, thous_sep);
        }
      }
    }

    // left, right
    if (right === '0') {
      // No decimal or fractional digits
      value = integer;
    } else {
      // '.'
      var dec_pt = monetary.mon_decimal_point;
      if (right === '' || right === undefined) {
        right = conversion === 'i' ? monetary.int_frac_digits : monetary.frac_digits;
      }
      right = parseInt(right, 10);

      if (right === 0) {
        // Only remove fractional portion if explicitly set to zero digits
        fraction = '';
        dec_pt = '';
      } else if (right < fraction.length) {
        fraction = Math.round(parseFloat(fraction.slice(0, right) + '.' + fraction.substr(right, 1))) + '';
        if (right > fraction.length) {
          fraction = new Array(right - fraction.length + 1)
            .join('0') + fraction; // prepend with 0's
        }
      } else if (right > fraction.length) {
        fraction += new Array(right - fraction.length + 1)
          .join('0'); // pad with 0's
      }
      value = integer + dec_pt + fraction;
    }

    var symbol = '';
    if (showCurrSymbol) {
      // 'i' vs. 'n' ('USD' vs. '$')
      symbol = conversion === 'i' ? monetary.int_curr_symbol : monetary.currency_symbol;
    }
    var sign_posn = neg ? monetary.n_sign_posn : monetary.p_sign_posn;

    // 0: no space between curr. symbol and value
    // 1: space sep. them unless symb. and sign are adjacent then space sep. them from value
    // 2: space sep. sign and value unless symb. and sign are adjacent then space separates
    var sep_by_space = neg ? monetary.n_sep_by_space : monetary.p_sep_by_space;

    // p_cs_precedes, n_cs_precedes // positive currency symbol follows value = 0; precedes value = 1
    var cs_precedes = neg ? monetary.n_cs_precedes : monetary.p_cs_precedes;

    // Assemble symbol/value/sign and possible space as appropriate
    if (flags.indexOf('(') !== -1) {
      // flag: parenth. for negative
      // Fix: unclear on whether and how sep_by_space, sign_posn, or cs_precedes have
      // an impact here (as they do below), but assuming for now behaves as sign_posn 0 as
      // far as localized sep_by_space and sign_posn behavior
      repl = (cs_precedes ? symbol + (sep_by_space === 1 ? ' ' : '') : '') + value + (!cs_precedes ? (
        sep_by_space === 1 ? ' ' : '') + symbol : '');
      if (neg) {
        repl = '(' + repl + ')';
      } else {
        repl = ' ' + repl + ' ';
      }
    } else {
      // '+' is default
      // ''
      var pos_sign = monetary.positive_sign;
      // '-'
      var neg_sign = monetary.negative_sign;
      var sign = neg ? (neg_sign) : (pos_sign);
      var otherSign = neg ? (pos_sign) : (neg_sign);
      var signPadding = '';
      if (sign_posn) {
        // has a sign
        signPadding = new Array(otherSign.length - sign.length + 1)
          .join(' ');
      }

      var valueAndCS = '';
      switch (sign_posn) {
        // 0: parentheses surround value and curr. symbol;
        // 1: sign precedes them;
        // 2: sign follows them;
        // 3: sign immed. precedes curr. symbol; (but may be space between)
        // 4: sign immed. succeeds curr. symbol; (but may be space between)
      case 0:
        valueAndCS = cs_precedes ? symbol + (sep_by_space === 1 ? ' ' : '') + value : value + (sep_by_space ===
          1 ? ' ' : '') + symbol;
        repl = '(' + valueAndCS + ')';
        break;
      case 1:
        valueAndCS = cs_precedes ? symbol + (sep_by_space === 1 ? ' ' : '') + value : value + (sep_by_space ===
          1 ? ' ' : '') + symbol;
        repl = signPadding + sign + (sep_by_space === 2 ? ' ' : '') + valueAndCS;
        break;
      case 2:
        valueAndCS = cs_precedes ? symbol + (sep_by_space === 1 ? ' ' : '') + value : value + (sep_by_space ===
          1 ? ' ' : '') + symbol;
        repl = valueAndCS + (sep_by_space === 2 ? ' ' : '') + sign + signPadding;
        break;
      case 3:
        repl = cs_precedes ? signPadding + sign + (sep_by_space === 2 ? ' ' : '') + symbol + (sep_by_space ===
          1 ? ' ' : '') + value : value + (sep_by_space === 1 ? ' ' : '') + sign + signPadding + (
          sep_by_space === 2 ? ' ' : '') + symbol;
        break;
      case 4:
        repl = cs_precedes ? symbol + (sep_by_space === 2 ? ' ' : '') + signPadding + sign + (sep_by_space ===
          1 ? ' ' : '') + value : value + (sep_by_space === 1 ? ' ' : '') + symbol + (sep_by_space === 2 ?
          ' ' : '') + sign + signPadding;
        break;
      }
    }

    var padding = width - repl.length;
    if (padding > 0) {
      padding = new Array(padding + 1)
        .join(' ');
      // Fix: How does p_sep_by_space affect the count if there is a space? Included in count presumably?
      if (flags.indexOf('-') !== -1) {
        // left-justified (pad to right)
        repl += padding;
      } else {
        // right-justified (pad to left)
        repl = padding + repl;
      }
    }
    return repl;
  };

  return format.replace(regex, doReplace);
}

function networkConnectionCheck(reachability) {
  if ( reachability.remoteHostStatus == NetworkStatus.NOT_REACHABLE ) {
    alert("You appear to be offline, drug and provider search will not work.", "Healthcare Marketplace");
  }
};