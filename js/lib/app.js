var app = {
	ads: function(i) {
		if(typeof i == 'undefined') {
			i = {
				"androidBanner": "ca-app-pub-6136220370460708/8002647475",
				"androidInterstitial": "",
				"iosBanner": "ca-app-pub-6136220370460708/1134221872",
				"iosInterstitial": ""
			}
		}
		var admobid = {};
		if( /(android)/i.test(navigator.userAgent) ) {
	    	admobid = { // for Android
	        	banner: $.trim(i.androidBanner),
	        	interstitial: $.trim(i.androidInterstitial)
		    };
		} else if(/(ipod|iphone|ipad)/i.test(navigator.userAgent)) {
	    	admobid = { // for iOS
        		banner: $.trim(i.iosBanner),
        		interstitial: $.trim(i.iosInterstitial)
		    };
		} else {
			/*
		    admobid = { // for Windows Phone
        		banner: 'ca-app-pub-6561619341024936/3661276608',
	        	interstitial: 'ca-app-pub-6561619341024936/5138009805'
		    };
		    */
		}		
	    if(!AdMob) {
	    	return;
	    }
	    
	    if(admobid.banner) {
    		AdMob.createBanner({
    			adId: admobid.banner,
    			overlap: false,
    			offsetTopBar: false,
    			position: AdMob.AD_POSITION.BOTTOM_CENTER,
    			bgColor: 'black',
    			autoShow:true
    		});	    	
	    }
	    
	    if(admobid.interstitial) {
	    	AdMob.prepareInterstitial({
        		adId: admobid.interstitial,
	        	autoShow: true
    		});
	    }
	},
	
	disableAds: function(){
		
	},
	
    initialize: function() {
        app.bindEvents();	
        Click('.extLink', app.openLink);
    },
	
    bindEvents: function() {
        document.addEventListener('deviceready', app.onDeviceReady, false);
		document.addEventListener("resume", app.onResume, false);
		document.addEventListener("offline", app.onOffline, false);
    },
	
	clearData: function(){
		window.key = '';
		localStorage.clear();
		app.logOut();
		global.init();
	},
	
	forgotPassword: function(){
		var email = prompt('What is your email?');
		if(email) {
			$.mobile.loading('show');
			var a = new API({
				db:false,
				data:{
					email:email,
					action:'forgotPassword'
				},
				error: function(){
					$.mobile.loading('hide');
				},
				success: function(){
					alert('Please check your email.');
					$.mobile.loading('hide');
				}
			});
		}
	},
	
	logOut: function(){
		var db = new simpleDB('user');
		db.del('data');
		window.key = '';
		global.ui();
		window.location.hash = 'menu';
	},

    onDeviceReady: function() {
    	if(String(device.platform).toLowerCase() == 'ios') {
    		StatusBar.hide();
    	}
		window.alert = navigator.notification.alert;
		window.open = cordova.InAppBrowser.open;
		disableAds.setup(function(){
			disableAds.restore(function(serveAds){
				if(serveAds) {
					var db = new simpleDB('meta');
					var myAds = db.get('ads');
					if(myAds) {
						app.ads(myAds);
					} else {
						var a = new API({
							db:false,
							data:{
								action:'ads'
							},
							error: function(){
								app.ads({
									"androidBanner": "ca-app-pub-6136220370460708/8002647475",
									"androidInterstitial": "",
									"iosBanner": "ca-app-pub-6136220370460708/1134221872",
									"iosInterstitial": ""
								});
							},
							success: function(response){
								db.put('ads', response.output);
								app.ads(response.output);
							}
						});
					}
				} else {
					$('#disableAds').remove();
				}
			});			
		});
		$('body').trigger('appReady');
		global.ui();
		$('body').addClass(String(device.platform).toLowerCase());
    },
    
    onOffline: function(){
    	alert('You appear to be offline, please connect to the network to search drug or provider data.');
    },
	
	onResume: function(){
		window.alert = navigator.notification.alert;
		global.ui();
		disableAds.setup();
		disableAds.restore(function(serveAds){
			if(serveAds) {
				app.ads();
			} else {
				$('#disableAds').remove();
			}
		});
		//admob.createBannerView({publisherId: "pub-6561619341024936"});
	},
	
	openLink: function(t){
		var ref = window.open($(t).attr('href'), '_blank', 'location=yes,hardwareback=no');
		ref.addEventListener('exit', function(ref){
			ref.close();
		});
	}
};
Click('#logOut', app.logOut);
Click('#clear', app.clearData);
Click('#forgotPassword', app.forgotPassword);

/*
$(document).on('appReady', 'body', function(){
	// Tracking and Analytics
	//analytics.startTrackerWithId('UA-68447725-2');
	var analytics = navigator.analytics;
	analytics.setTrackingId('UA-68447725-2');
	$(document).on( "pagechange" , function(e, data) {
		var page = $('.ui-page-active').find('h1.ui-title').text();
		//analytics.trackView(page);
		analytics.sendAppView(page);
	});
	$(document).on('register', 'body', function(){
		//analytics.trackEvent('User Actions', 'Completed Registration');
		analytics.sendEvent('User Actions', 'Completed Registration');
	});
	$(document).on('logIn', 'body', function(){
		//analytics.trackEvent('User Actions', 'Logged In');
		analytics.sendEvent('User Actions', 'Logged In');
	});			
	$(document).on('createdRating', 'body', function(){
		//analytics.trackEvent('Created Rating', 'Logged In');
		analytics.sendEvent('Created Rating', 'Logged In');
	});
	$(document).on('click', '#contact a', function(){
		var data = deparam($('#theResult').attr('data-uri'));
		analytics.sendEventWithParams('User Actions', 'Contact Provider', 'Contact From App Search', 0, {'dimension1':data.wp_id});
	});
	$(document).on('submit', '#home form', function(){
		var data = $(this).serializeObject();
		analytics.sendEventWithParams('User Actions', 'Search', 'App Search', 0, {'dimension1':data.term});
	})
});
*/