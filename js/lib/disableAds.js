var disableAds = {
    androidApplicationLicenseKey:"MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEArGG14kgkqKW5a973611jXpwFDL/hBd4iRaKOQAUmVV0mr8XLc4t/TiVH0KKl0m3oqJ+29O4xvr8QmfIEyWaVSx0m/3RZVydgrJbQLkNptlV+Yj+qXcRd/oVDFg+v7YH3CDC+LtnCmOInB3WIqbFIMTKmbMrDgwb6vPHppcOgVef+CqnQGcWUBdYWaOLvRvwfXirFuFFGbu4yWUbHJDh7VI8intuBT4YSBdKmGDAxclui5cQ2UfUdNySzMnEhR8z/ZYTH8ecoTmNRFDC/mAIIniOY26d0LYmFqoPF/NQwESgwTjzse6PCuDJH9S8gOjOhAWOugzNyckuxdiz+ua6YNwIDAQAB",
    productIds:"hm_disable_ads",
    existing_purchases:[],
    product_info:{},
    
    consume: function(productId){
        //consume product id, throw away purchase product id info from server. 
        window.iap.consumeProduct(productId, function (result){
            // alert("purchaseProduct");
        }, 
        function (error){
            alert("error: "+error);
        });	        
    },
    
    hasProduct: function(productId){
        return disableAds.existing_purchases.indexOf(productId) !== -1;        
    },
    
    purchase: function(productId){
        //purchase product id, put purchase product id info into server. 
        window.iap.purchaseProduct(productId, function (result){
            // alert("purchaseProduct");
        }, 
        function (error){
            // alert("error: "+error);
        });        
    },
    
    restore: function(callback){
        var serveAds = true;
        //get user's purchased product ids which purchased before and not cunsumed. 
        window.iap.restorePurchases(function(result){
            if(result.length) {
                $.each(result, function(key, value){
                    console.log(value);
                    if(value.productId == 'hm_disable_ads') {
                        if(typeof callback == 'function') {
                            serveAds = false;
                        }
                        return;
                    }
                });
            }
            if(typeof callback == 'function') {
                callback(serveAds);
            }
        }, function (error){
            alert("error: "+error);
        });        
    },
    
    setup: function(callback){
        window.iap.setUp(disableAds.androidApplicationLicenseKey);
        window.iap.requestStoreListing(disableAds.productIds, function (result){
            for (var i = 0 ; i < result.length; ++i){
                var p = result[i];
                disableAds.product_info[p["productId"]] = {
                    title: p["title"], 
                    price: p["price"]
                };
            }
            if(typeof callback == 'function') {
                callback();
            }            
        }, function (error){
            // alert("error: "+error);
        });
    }
};